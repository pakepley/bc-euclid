setup_dir_flag         = false;
define_problem_flag    = false;
generate_dom_dims_flag = false;
generate_discr_flag    = false;
run_solver_flag        = false;
init_inversion_flag    = false;
init_wvfld_rec_flag    = false;
recover_coords_flag    =  true;

%% Include some helper functions and raytracers
addpath(['./io_facilities']);
addpath(['./compute_bdf']);
addpath(['./ray_trace']);
addpath(['./half_space']);
addpath(['./wvfld_rec']);

src_dir = pwd();
user_home = getenv('HOME');

save_base = [user_home '/Computational_Data'];
if not(exist(save_base,'dir'))
    mkdir(save_base);
end

save_root = [save_base '/wvfld_rec_project'];
if not(exist(save_root, 'dir'))
    mkdir(save_root);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setup_dir_flag    
    save_path = [save_root '/' timestamp()];    
    mkdir(save_path);
    
    %% Save a snapshot of the source code
    save_source_code(save_path);
elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');
    load([save_path '/discr.mat']);
    addpath(['./' discr.domain_type]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     Define the problem                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if define_problem_flag
    if exist([save_path '/problem_definition.mat'])
        load([save_path '/problem_definition.mat']);
    else
        %% Use if conte is available:
        % data_type          =   'ND';
        % start_type         = 'safe';
        % x_src_separation   = 0.0250;
        % x_rec_separation   = 0.0125;
        % L_src              =    3.0;
        % L_rec              =    4.0; 
        % t_separation       = 0.0250;
        % T                  =    1.5;
        % n_processors       =     12;    
        % c_code             = '1.0-0.5*exp(-16.0*((x[0]*x[0]) + (x[1]-0.5)*(x[1]-0.5)))';
        % transl_invar_flag  =  false;
        % domain_type        = 'half_space'; % disk does not work
        data_type          =   'ND';
        start_type         = 'safe';
        x_src_separation   =  0.050;
        x_rec_separation   =  0.025;
        L_src              =    2.0;
        L_rec              =    3.5; 
        t_separation       =  0.050;
        T                  =    1.5;
        n_processors       =     12;    
        %c_code             = '1.0-0.5*exp(-16.0*((x[0]*x[0]) + (x[1]-0.5)*(x[1]-0.5)))';
        %c_code             = '1.0+0.5*exp(-16.0*((x[0]*x[0]) +
        %(x[1]-0.5)*(x[1]-0.5)))';
        c_code             = '1.0+0.5*exp(-64.0*((x[0]*x[0])))';
        transl_invar_flag  =  false;
        domain_type        = 'half_space'; % disk does not work

        Ctrue=@(x,y)( 1.0+0.5*exp(-64.0*((y-0.5).*(y-0.5))));
        
        % save to file:
        save([save_path '/problem_definition.mat'], 'data_type', ...
             'start_type', 'x_src_separation', 'x_rec_separation', ...
             'L_src', 'L_rec', 't_separation', 'T', 'n_processors', ...
             'c_code', 'transl_invar_flag', 'domain_type');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Ray - trace to determine domain size                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if generate_dom_dims_flag
    if not(exist([save_path '/domain_extrema.mat']))
        load([save_path '/problem_definition.mat']);
        write_extrema_mat_file(save_path, L_src,  x_src_separation, ...
                               T, c_code, transl_invar_flag, n_processors);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Write the discretization to file                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if generate_discr_flag
    if exist([save_path '/discr.mat'])
        error(['The discr has already been defined. Turn off the ' ...
               'generate_discr_flag.'])
    else
        discr = init_params(save_path, data_type, ...
                            start_type, T, t_separation, ...
                            L_src, L_rec, ...
                            x_src_separation, x_rec_separation, ...
                            c_code, n_processors, transl_invar_flag)            
    end
        
    save_femparams(discr, save_path);
    discr = basis_x_normalizations(discr);       
    save([save_path '/discr.mat'], 'discr');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_solver_flag
  diary([save_path '/fem_diary.txt']);

  if not(exist('discr','var'))
      % get the discr from the save_path location
      load([save_path '/discr.mat']);
  end

  %run_femsolver(discr, save_path, 'all_probe_mode', [save_path '/sol']);
  run_femsolver(discr, save_path, 'trace_probe_mode');
  diary off;
end 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%             Construct the matrices for the inversion                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if init_inversion_flag
    if not(exist('discr','var'))
        % get the discr from the save_path location
        load([save_path '/discr.mat']);
    end
        
    discr = init_inversion(discr, save_path);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%      Build the wavefield recovery sampling functions (matrix)       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if init_wvfld_rec_flag
   
    alpha  = 1.0 * 10^-5; % Tikhonov regularization parameter

    h = 2.0 * discr.t_separation;
    ilo = discr.mx_src - 15;
    ihi = discr.mx_src + 15;
    jlo = 1;
    jhi = floor(discr.mt_src);

    ranges_for_coords = {};
    ranges_for_coords.ilo = ilo;
    ranges_for_coords.ihi = ihi;
    ranges_for_coords.jlo = jlo;
    ranges_for_coords.jhi = jhi;
    ranges_for_coords.nxx = ihi - ilo + 1;
    ranges_for_coords.nyy = jhi - jlo + 1;

    S = compute_wvfld_recon_sampling_matrix(discr, alpha, save_path, h, ...
                                            ilo, ihi, jlo, jhi);
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                Run the wavefield recovery step                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if recover_coords_flag
    Lambda = read_femtrace(discr, save_path);

    % estimate the coordinate transform
    [xs_est, ys_est, x_bndry_ints, y_bndry_ints] = ...
        estimate_coord_transform(discr, ranges_for_coords, ...
                                 Lambda, S);

    % estimate the wavespeed (also velocity vectors)
    p_in = [];
    [c_est, dx_ds, dy_ds, xs_int, ys_int] = ...
        compute_wavespeed(discr, ranges_for_coords, xs_est, ys_est, p_in);

    % make space to save the output
    output_root = [save_path '/wvfld_rec_data'];    
    output_root = sprintf([output_root '/caps_h_%.04f_alpha_%.04e'], h, alpha);
    results_root = sprintf([output_root '/results_i_%d_%d_j_%d_%d'], ...
                           ilo, ihi, jlo, jhi);
    conditional_mkdir(results_root);

    % load the discr without the matrices to save along with the data
    discr2=load([save_path '/discr.mat']); 
    discr2=discr2.discr;
    os = ranges_for_coords;
    save([results_root '/results.mat'], ...
         'discr2', 'xs_est', 'ys_est', 'xs_int', 'ys_int', ...
         'os', 'c_est', 'dx_ds', 'dy_ds', 'x_bndry_ints', 'y_bndry_ints', ...
         'p_in');
    
    % generate images
    make_wvfld_rec_images(results_root, Ctrue)
end