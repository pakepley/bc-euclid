import sys, os
import numpy as np
import scipy.io

def parse_pvtu(directory, pvtu_file_name):
	pvtu_file_path = directory + '/' + pvtu_file_name

	pvtu_file = open(pvtu_file_path,'r')
	pvtu_file_list = list(pvtu_file)
	pvtu_file.close()
	
	vtu_files = [line for line in pvtu_file_list if 'Piece Source' in line]
	vtu_files = map(lambda x: x.split('"')[1], vtu_files)
	
	Xs = np.array([])
	Ys = np.array([])
	Zs = np.array([])

	for f in vtu_files:
		Xsf, Ysf, Zsf = parse_vtu(directory, f)
		Xs = np.append(Xs,Xsf)
		Ys = np.append(Ys,Ysf)
		Zs = np.append(Zs,Zsf)

	return (Xs,Ys,Zs)

def parse_vtu(directory, vtu_file_name):
	
	vtu_file_path = directory + '/' + vtu_file_name

	# open the vtu_file
	vtu_file = open(vtu_file_path,'r')
	vtu_file_list = list(vtu_file)
	vtu_file.close()
		
	# get the point locations
	Points_Found = False
	ip = 0
	while not Points_Found:
		Points_Found = 'Points' in vtu_file_list[ip]
		ip = ip + 1
	Points = vtu_file_list[ip+1].split('>')[1]
	Points = Points.split('<')[0]
	Points = Points.split(' ')
	Points = [float(x) for x in Points if x]
	del Points[2::3]
	n_ps = len(Points) / 2
	Points = np.array(Points).reshape((n_ps, 2))
	Xs, Ys = Points[:,0], Points[:,1]


	# get the values of the function at the points
	Point_Data_Found = False
	ipd = 0
	while not Point_Data_Found:
		Point_Data_Found = 'PointData' in vtu_file_list[ipd]
		ipd = ipd + 1
	PointData = vtu_file_list[ipd].split('>')[1]
	PointData = PointData.split('<')[0]
	PointData = PointData.split(' ')
	Zs = np.array([float(x) for x in PointData if x])

	return (Xs,Ys,Zs)


def save_as_mat(file_path):
	# get the directory
	directory = file_path.split('/')
	file_name = directory.pop(-1)
	directory = '/'.join(directory)

	if '.vtu' in file_name:
		file_root = file_name.strip('.vtu')
		Xs, Ys, Zs = parse_vtu(directory, file_name)
	elif '.pvtu' in file_name:
		file_root = file_name.strip('.pvtu')
		print file_root
		Xs, Ys, Zs = parse_pvtu(directory, file_name)
	
	output_path = directory + '/' + file_root + '.mat'
	# save a matfile with the point data
	scipy.io.savemat(output_path, mdict={'Xs' : Xs, 'Ys' : Ys, 'Zs' : Zs}, oned_as='row')

if __name__ == '__main__':	
	file_path = sys.argv[1]
	save_as_mat(file_path)
