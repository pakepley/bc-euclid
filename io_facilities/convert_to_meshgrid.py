import sys
import scipy.io as sio
import numpy as np
from scipy.interpolate import griddata

def interpolate_radial_stuff(Xs,Ys,Zs, r_in, nr, ntheta):
	Ps = np.zeros((np.size(Xs), 2))
	Ps[:,0] = Xs
	Ps[:,1] = Ys
	
	r  = np.linspace(r_in, 1.0, nr)
	th = np.linspace(0, 2*np.pi, ntheta)
	RR,TTh = np.meshgrid(r,th)
	XX, YY = RR * np.cos(TTh), RR* np.sin(TTh)
	
	grid_z0 = griddata(Ps, Zs, (XX, YY), method='cubic')
	ZZ = grid_z0.T
	ZZ = np.transpose(ZZ)
	
	return (XX,YY,ZZ)

def interpolate_rectangular_stuff(Xs,Ys,Zs):
	Ps = np.zeros((np.size(Xs), 2))
	Ps[:,0] = Xs
	Ps[:,1] = Ys
	
	x = np.unique(Xs)
	y = np.unique(Ys)
	XX, YY = np.meshgrid(x,y)
	
	NZZ = np.shape(Zs)[0]	
	ZZ = np.zeros((NZZ, np.shape(XX)[0], np.shape(XX)[1]))

	for i in range(0,NZZ):
		print i
		grid_z0 = griddata(Ps, Zs[i,:], (XX, YY), method='cubic')
		ZZ_i = grid_z0.T
		ZZ_i = np.transpose(ZZ_i)
		ZZ[i,:] = ZZ_i
	
	return (XX,YY,ZZ)

def save_as_mat(in_mat_file, mode):
	# get the directory
	if '/' in in_mat_file:
		directory = in_mat_file.split('/')
		in_file_name = directory.pop(-1)
		directory = '/'.join(directory)
	else:
		in_file_name = in_mat_file
		directory = './'
	
	# where will we save the output?
	out_mat_file = '{0}/{1}_interp.mat'.format(directory,
											   in_file_name.replace('.mat',''))
	
	# read in the matfile data
	print in_mat_file
	mat_contents = sio.loadmat(in_mat_file)
	
	Xs = mat_contents['Xs']
	Ys = mat_contents['Ys']
	Zs = mat_contents['Zs']	

	if mode == 'rectangular':
		XX, YY, ZZ = interpolate_rectangular_stuff(Xs,Ys,Zs)
	elif mode == 'polar':		
		XX, YY, ZZ = interpolate_radial_stuff(Xs,Ys,Zs, 0.5, 100, 600)

	sio.savemat(out_mat_file, mdict={'Xs' : XX, 'Ys' : YY, 'Zs' : ZZ}, oned_as='row')


if __name__ == '__main__':
	in_mat_file = sys.argv[1]
	mode = sys.argv[2]	
	save_as_mat(in_mat_file, mode)
