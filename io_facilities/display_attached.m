function display_attached_flag = display_attached()
%% --------------------------------------------------------------------
%% Function: display_attached()
%% Use: determine if a display is attached. used by plotting utilities
%% --------------------------------------------------------------------
%% Inputs: NONE
%% --------------------------------------------------------------------
%% Outputs:
%% running_octave_flag  - a boolean which is true if running octave
%% --------------------------------------------------------------------


    try
        %% this works on matlab 2012b at least
        screen_size = get( 0, 'Screensize' );
        display_attached_flag =  screen_size(3) ~= 1;
    end

    try
        %% this should work on matlab 2014(?)
        screen_size = get( groot, 'Screensize' );
        display_attached_flag =  screen_size(3) ~= 1;
    end

end