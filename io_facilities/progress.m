function progress(l,m)
%% --------------------------------------------------------------------
%% Function: progress
%% Use:      handy function to display what percentage has been 
%%           completed
%% --------------------------------------------------------------------
%% Inputs:
%% l    - the current step
%% m    - the total number of steps
%% --------------------------------------------------------------------
    
    if l == ceil(.01 * m)
        disp(' 1% done');
    elseif l == ceil(.25 * m)
        disp('25% done');
    elseif l == ceil(.50 * m);
        disp('50% done');
    elseif l == ceil(.75 * m);
        disp('75% done');
    elseif l == ceil(.99 * m);
        disp('99% done');
    end
    
    %% Force it to print to screen. Particularly annoying in octave.
    if running_octave()
        fflush(stdout);
    end
end