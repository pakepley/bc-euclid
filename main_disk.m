%% flags to tell us what gets run and when
init_discr_flag      = false;
run_fem_solver_flag  = false;
compute_matrix_flag  = false;
run_wvfld_recov_flag =  true;

%% the parent directory for all save information
%% default is to point to $HOME/junk
save_root = [getenv('HOME') '/moving_data_data'];
if not(exist(save_root,'dir'))
  mkdir(save_root)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if init_discr_flag
        
    %% Set up the parameters
    T                  =         2.00 ;
    t_separation       =         0.10 ;
    L                  =         2*pi ;
    spatial_separation =         0.10 ;
    data_type          =         'ND' ;
    n_processors       =          18  ;
    domain_type        =       'disk' ;    
    y                  =         1.00 ;
    
    addpath(['./' domain_type]);    
    if strcmp('domain_type', 'half_space')
        discr = init_params(T, t_separation, L, spatial_separation, ...
                            data_type, n_processors, y);    
    else
        discr = init_params(T, t_separation, L, spatial_separation, ...
                            data_type, n_processors);    
    end

    %% prep for storage
    save_path = [save_root '/' data_type '_' timestamp()];
    mkdir(save_path);

    %% save the discretization for later
    save([save_path '/discr.mat'], 'discr'); % save workspace to be re-read

elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');
    load([save_path '/discr.mat']);
end

addpath(['./' discr.domain_type]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_fem_solver_flag    
    % Save the source files so that we can reproduce the simulations
    % zip([save_path '/src.zip'], {'*.m', '*.py'});

    % Save the command window output
    diary([save_path '/diary_fem.txt']);

    save_femparams(discr, save_path);
    
    % run the solver in forward mode
    run_femsolver(discr, save_path);

    % export the diary 
    diary off;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   Compute matrices used in approximately solving control problems   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if or( compute_matrix_flag , run_wvfld_recov_flag)
    if or( not(isfield('discr','coeffs_K')), ... 
           not(isfield('discr','coeffs_Ginv')) )
        discr = get_matrices(discr,save_path);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    Solve control problem Kh = Kf, where f is a long-time source     %%
%%    function and h has a much smaller temporal support               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_wvfld_recov_flag
    output_root = [save_path '/wavefield_recovery'];
    if not(exist(output_root,'dir'))
        mkdir(output_root);
    end
    output_root = [output_root '/' timestamp()];   
    if not(exist(output_root,'dir'))
        mkdir(output_root);
    end
    
    for i = 0 : (discr.nt_src + 1)
        
        output_path = [output_root sprintf('/time_%f', discr.T - i*discr.t_separation) ];    
        discr.output_path = output_path;
        if not(exist(output_path,'dir'))
            mkdir(output_path);
        end
        %zip([output_path '/src.zip'], {'*.m', '*.py'});

        t_c     = 0.25 + discr.t_separation * i;
        p_c     = 0.0;
        sigma_h = 0.1;
        [rhs_integral, f_coeffs] = generate_test_data(discr, t_c, p_c, sigma_h);
        
        % Control set \Gamma, we currently use full boundary
        R    =                     .5   ; 
        tau  =      make_tau(discr, R)  ;
        mask =    make_mask(discr, tau) ;
        chi  =            double(mask)  ;

        % Control Prob solver params
        Nouter   =   300;    % maximum n outer GMRES iters
        Nrestart =    20;    % maximum n inner GMRES iters
        tol      =  1e-6;    % tolerance for GMRES 
        alpha    =  0.0001;    % Tikhonov regularization parameter

        %% Coefficients of the control problem's solution
        h_coeffs = solve_bc(discr, mask, rhs_integral, alpha, Nouter, Nrestart, ...
                            tol);

        % Save the coefficients
        f_coeff_path = save_coeffs(discr, save_path, f_coeffs, output_path, 'coeffs_f');
        h_coeff_path = save_coeffs(discr, save_path, h_coeffs, output_path, 'coeffs_h');
        diff_coeff_path = save_coeffs(discr, save_path, f_coeffs - h_coeffs, ...
                                      output_path, 'coeffs_diff');

        % Compute the difference wave-field    
        run_femsolver(discr, save_path, f_coeff_path);
        run_femsolver(discr, save_path, h_coeff_path);
        run_femsolver(discr, save_path, diff_coeff_path);
    end
    
    % write pvd files to show time stepping
    system(['python write_time_series_pvd.py ' output_root]);
end
