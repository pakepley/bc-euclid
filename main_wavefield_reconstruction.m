setup_dir_flag         = false;
run_solver_flag        = false;
init_inversion_flag    = false;
wavefield_recovery     = false;
coarse_basis_test      =  true;

init_inversion_flag = init_inversion_flag || coarse_basis_test;

%% Include some helper functions and raytracers
addpath(['./io_facilities']);
addpath(['./compute_bdf']);
addpath(['./half_space']);
addpath(['./ray_trace']);
addpath(['./wvfld_reconstruction']);
addpath(['./wvfld_reconstruction_perfect_data']);

src_dir = pwd();
user_home = getenv('HOME');

save_base = [user_home '/Computational_Data'];
if not(exist(save_base,'dir'))
    mkdir(save_base);
end

save_root = [save_base '/wavefield_recovery'];
if not(exist(save_root, 'dir'))
    mkdir(save_root);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setup_dir_flag    
    save_path = [save_root '/' timestamp()];    
    mkdir(save_path);
    
    %% Save a snapshot of the source code
    save_source_code(save_path);

    % save parameters and directory
    if not(exist([save_path '/discr.mat'], 'file'))
        data_type          =    'ND';
        start_type         = 'early';
        spatial_separation =  0.0125;
        L                  =     2.0;
        t_separation       =  0.0125;
        T                  =    1.25;
        n_processors       =      22;
        c_code             =   '1.0';
        %c_code             =   '1 + x[1]';
        transl_invar_flag  =    true;
        domain_type        = 'half_space';
        
        
        addpath(['./' domain_type]);    
        if strcmp(domain_type, 'half_space')
            discr = init_params(save_path, data_type, start_type, T, ...
                                t_separation, L, spatial_separation, c_code, ...
                                n_processors, transl_invar_flag);
        else
            error('Disk case not yet implemented');
        end
        
        save_femparams(discr, save_path);
        discr = basis_x_normalizations(discr,save_path);       
        save([save_path '/discr.mat'], 'discr');
    end
elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');

    load([save_path '/discr.mat']);
    addpath(['./' discr.domain_type]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_solver_flag
  diary([save_path '/fem_diary.txt']);

  if not(exist('discr','var'))
      % get the discr from the save_path location
      load([save_path '/discr.mat']);
  end

  %run_femsolver(discr, save_path, 'all_probe_mode', [save_path '/sol']);
  run_femsolver(discr, save_path, 'trace_probe_mode');
  diary off;
end 


if init_inversion_flag
    if not(exist('discr','var'))
        % get the discr from the save_path location
        load([save_path '/discr.mat']);
    end

    discr = get_matrices(discr, save_path);
end




if wavefield_recovery
   
    Nouter = 300;
    Nrestart = 20;
    tolerance = 1e-6;   % the GMRES tolerance
    alpha  = 0.00001;	% Tikhonov regularization parameter
    discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, ...
                                                   tolerance); 

    xc = 0.0;
    tc = (discr.T - discr.T/4.0);
    rescale = 64.0;
    f_coeffs = generate_wvfld_rec_test(discr, xc, tc, rescale);    
    

    h = 4.0 * discr.t_separation;
    ilo = discr.mx_src - 15;
    ihi = discr.mx_src + 15;
    jlo = 1;
    jhi = discr.mt_src;
    u = wvfld_recon_sampling(discr, discr_inversion, f_coeffs, save_path, ...
                             h, ilo,ihi,jlo,jhi);
    save('~/Dropbox/u.mat', 'u', 'xc','tc','rescale', 'f_coeffs', 'h', ...
         'ilo','ihi', 'jlo', 'jhi');

    [xs_sol_padded, ys_sol_padded] = get_basic_sol_padding(discr);
    save('~/Dropbox/plot_coords.mat', 'xs_sol_padded','ys_sol_padded');

    load(sprintf([save_path '/sol/sol_%d.mat'], discr.mx_src));
    plot_sol_from_probes(discr, sol, f_coeffs, discr.mt_rec, ...
                         '~/Dropbox', 'test');

end

if coarse_basis_test
    
    sol_path = [save_path '/sol'];
    if not(exist(sol_path, 'dir'))
        conditional_mkdir(sol_path);
        run_femsolver(discr, save_path, 'sol_probe_mode');
    end


    Nouter = 300;
    Nrestart = 20;
    tolerance = 1e-6;   % the GMRES tolerance
    alpha  = 0.00001;	% Tikhonov regularization parameter
    discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, ...
                                                   tolerance); 
    
    h = 2.0 * discr.t_separation;
    ilo = discr.mx_src - 32;
    ihi = discr.mx_src + 32;
    jlo = 1;
    jhi = 36;

    if not(exist('S','var'))
        fprintf('Computing sampling matrix S...\n');
        S = compute_wvfld_recon_sampling_matrix(discr, discr_inversion, ...
                                                save_path, h, ilo, ihi, ...
                                                jlo, jhi);
        fprintf('Done!\n');
    end

    dilation =  4.0;

    if not(exist('discr_coarse', 'var')) || discr_coarse.dilation ~= ...
            dilation
        
        coarse_save_path = sprintf([save_path '/test_%f'], dilation);
        conditional_mkdir(coarse_save_path);
        discr_coarse = init_params(coarse_save_path, ...
                                   discr.data_type, ...
                                   discr.start_type, ...
                                   discr.T, ... 
                                   dilation * discr.t_separation, ...
                                   discr.L_src, ... 
                                   dilation * discr.x_separation, ...
                                   discr.c_code, ...
                                   1, ...
                                   discr.transl_invar_flag, ...
                                   discr.Y);
        discr_coarse.dilation = dilation;
                
        save_femparams(discr_coarse, coarse_save_path);

        discr_coarse = basis_x_normalizations(discr_coarse, coarse_save_path);       
        save([coarse_save_path '/discr_coarse.mat'], 'discr_coarse');

    end

    [U, iloc, ihic, jloc, jhic, U2] = ...
        compute_downsample_sampling_matrix(discr, discr_coarse, save_path, S, ...
                                           ilo, ihi, jlo, jhi);

    [Utt, iloc, ihic, jloc, jhic, U2tt] = ...
        compute_downsample_2nd_deriv_sampling_matrix(discr, discr_coarse, ...
                                                     save_path,S, ilo, ...
                                                     ihi, jlo, jhi);

    [Ucf, iloc, ihic, jloc, jhic, Ucf2] = compute_coarse_to_fine_sampling(discr, ...
                                                      discr_coarse, ...
                                                      save_path, S, ...
                                                      ilo,ihi,jlo,jhi);
                                        
        
    [ff,psi] = control_into_psi(discr, discr_coarse, U, discr_coarse.mx_src, jloc, jhic, iloc, ihic, (2*h)^(1/4));
end
