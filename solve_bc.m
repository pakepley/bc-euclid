function [f, mask] = solve_bc(o, alpha, rhs_integral, tau)
%% --------------------------------------------------------------------
%% Function: solve_bc
%% Use:      solve the boundary control problem using the function tau
%%           as a support constraint
%% --------------------------------------------------------------------
%% Inputs:
%% o       - the bcm discretization structure
%% alpha   - regularization parameter
%% tau     - the supporting function tau sampled on o.xs_src
%% --------------------------------------------------------------------
%% Outputs:
%% f       - the source which approximately solves: 
%%                    K f = rhs_integral
%% mask    - the mask used to enforce the support constraint arising
%%           from tau
%% --------------------------------------------------------------------
    
    % Logical matrix, used to select entries
    mask = make_mask(o, tau);

    % How many elements?
    if strcmp(o.domain_type, 'disk')
        N = o.nt_src * o.ntheta_src;
    elseif strcmp(o.domain_type, 'half_space')
        N = o.nt_src * o.nx_src;
    end

    function out = unmask(z)
        out = zeros(N, 1);
        out(mask) = z;
    end    
    
    NNZZ = nnz(mask);
    KK_alpha = o.coeffs_K(mask, mask); % Mask
    KK_alpha = KK_alpha + alpha * eye(NNZZ);
                
    % inputs to A must be masked, also outputs (ie f) will be masked, so no loss here 
    rhs_integral = rhs_integral(mask);

    %% compute the coefficients of z.
    f = KK_alpha\rhs_integral ;
    
    f = unmask(f);
end
