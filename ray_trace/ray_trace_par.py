import os, sys
import numpy as np
import scipy.io as sio

## numerical estimate of the gradient of c
def grad_c(x,dx):
	dim = len(x)
	k = np.zeros(np.size(x))
	grad_c = np.zeros(np.size(x))

	for i in range(0,dim):
		k[i] = dx
		grad_c[i] =  (-c(x+2.0*k) + 8.0*c(x+k) - 8.0*c(x-k) + c(x-2.0*k))/(12. * dx)
		k[i] = 0    
	return grad_c

## Hamiltonian resulting from wave-speed
def HAMIL(x,xi):	
	return 0.5 * (c(x)**2 * np.inner(xi,xi) -1)

# x-gradient of Hamiltonian
def dHAMIL_dxi(x,xi):
	return c(x)**2 * xi

## xi-gradient of Hamiltonian, numerically estimated
def dHAMIL_dx(x,xi,dx):
	dim = len(x)
	k = np.zeros(np.size(x))
	dHAMIL_dx = np.zeros(np.size(x))

	for i in range(0,dim):
		k[i] = dx
		dHAMIL_dx[i] = (-HAMIL(x+2.0*k,xi) + 8.0*HAMIL(x+k,xi) - 8.0*HAMIL(x-k,xi) + HAMIL(x-2.0*k,xi))/(12.0 * dx)		
		k[i] = 0
	return dHAMIL_dx

## norm of covector in metric g = c^{-2}e, so:
##        \|xi\|_g = c(x) \|xi\|_e
def covec_norm_g(x,xi):
	return c(x) * np.sqrt(np.inner(xi,xi))

## normalize a covector
def g_normalize_covec(x,xi):
	return (1.0 / covec_norm_g(x,xi)) * xi

# http://en.wikipedia.org/wiki/Eikonal_equation
def solve_hamilton_jacobi(x0,xi0,n,T):
	dim = len(x0)
	xs = np.zeros((n,dim))
	
	# dx for taking gradients
	dx = .001

	travel_time   = 0.0	
	abs_max_hamil = 0.0	
	travel_time   = 0.0
	
	# the step-size and it's multiples
	h        =  T / float(n)
	half_h   =  0.5 * h
	h_over_6 =  h / 6.0
	
 	# initialize
	x_i  = x0[:]
	xi_i = xi0[:]
		
	for i in range(n):
		if x_i[1] < 0.0:
			break
		
		# following line is for de-bugging
		if abs(HAMIL(x_i,xi_i)) > abs_max_hamil :
			abs_max_hamil = abs(HAMIL(x_i,xi_i))
			
		travel_time = travel_time + h
		
		kx_1  =  dHAMIL_dxi(x_i,xi_i)
		kxi_1 = -dHAMIL_dx (x_i,xi_i, dx)
		
		kx_2  =  dHAMIL_dxi(x_i + half_h * kx_1, xi_i + half_h * kxi_1)
		kxi_2 = -dHAMIL_dx (x_i + half_h * kx_1, xi_i + half_h * kxi_1, dx)
		
		kx_3  =  dHAMIL_dxi(x_i + half_h * kx_2, xi_i + half_h * kxi_2)
		kxi_3 = -dHAMIL_dx (x_i + half_h * kx_2, xi_i + half_h * kxi_2, dx)
		
		kx_4  =  dHAMIL_dxi(x_i + h * kx_3, xi_i + h * kxi_3)
		kxi_4 = -dHAMIL_dx (x_i + h * kx_3, xi_i + h * kxi_3, dx)
		
		x_i =   x_i + h_over_6 * (kx_1  + 2*kx_2  + 2* kx_3  + kx_4 ) 
		xi_i = xi_i + h_over_6 * (kxi_1 + 2*kxi_2 + 2* kxi_3 + kxi_4)

		xs[i,:] = x_i

	return xs

def getranges(xs):
	xlo = np.min(xs[:,0,:])
	xhi = np.max(xs[:,0,:])
	ylo = np.min(xs[:,1,:])
	yhi = np.max(xs[:,1,:])
	return xlo,xhi,ylo,yhi

def demo(x0, nthetas, n, T):
	xs = np.zeros((n,2,nthetas+1))
	for i in range(nthetas + 1):
		xi0 =  np.array([np.cos( (np.pi * i) / nthetas), np.sin((np.pi * i) / nthetas)])
		xi0 =  g_normalize_covec(x0,xi0)		
		xs[:,:,i]  =  solve_hamilton_jacobi(x0,xi0,n,T)				
	return xs

def cArr(X,Y):
	if np.shape(X) == np.shape(Y):
		ni,nj = np.shape(X) 
		ZZ    = np.zeros((ni,nj))
		for i in range(ni):
			for j in range(nj):
				ZZ[i,j] = c([X[i,j], Y[i,j]])
		return ZZ

if __name__ == '__main__':
	import matplotlib.pyplot as plt

	# get the simulation save_path:
	data_path = sys.argv[1]
	sys.path.append(data_path)
	
	from wave_speed import c
	
	## run the demo:
	x0 = np.array([0.2,0.0])	
	ntheta = 10
	n      = 100
	T      = 1.0
	xs = demo(x0, ntheta, n, T)
	xlo,xhi,ylo,yhi = getranges(xs)

	sio.savemat('{0}/geodesics.mat'.format(data_path), {'xs' : xs})

	# ## plot the geodesics
	# fig = plt.figure()
	# plt.axis([xlo,xhi,ylo,yhi])
	# for i in range(ntheta+1):
	# 	plt.plot(xs[:,0,i], xs[:,1,i])
	# 	px,py = x0
	# 	qx,qy = xs[-1,:,i]
	# 	dd = np.arccosh( 1.0 + ( (px - qx)**2 + (py - qy)**2) / (2.0 * (py + 1) * (qy + 1)) )		
	# fig.savefig('geodesics.png')
	
	# ## plot the wave-speed
	# from mpl_toolkits.mplot3d import Axes3D
	# from matplotlib import cm
	# fig = plt.figure()
	# ax = fig.gca(projection='3d')
	# #ax = fig.add_subplot(111, projection='3d')	
	# XX,YY = np.meshgrid(np.linspace(xlo,xhi,100), np.linspace(ylo,yhi,100))					
	# ZZ    = cArr(XX,YY)
	# ax.plot_surface(XX,YY,ZZ,rstride=1,cstride=1,linewidth=0,cmap =cm.coolwarm)
	# fig.savefig('wavespeed.png')
