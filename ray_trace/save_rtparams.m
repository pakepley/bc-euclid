function save_rtparams(save_path, xs_src, T, c_code, transl_invar_flag)
%% --------------------------------------------------------------------
%% Function: save_rtparams(o, save_path)
%% Use:      save parameters for the ray_tracer. The data is saved
%%           in the file: 
%%                     [save_path]/ray_trace_params.py
%%           the wave_speed is saved in the file:
%%                     [save_path]/wave_speed.py
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% save_path  - where the data was saved
%% --------------------------------------------------------------------

% Write the file (no mathematics below this point)
paramsfilename = [save_path '/ray_trace_params.py'];

fout = fopen(paramsfilename, 'w');

xs_src_str = sprintf('%g, ', xs_src);
xs_src_str = ['[' xs_src_str(1:end-2) ']'];

fprintf(fout, [...
    'T = %g\n' ...
    'xs_src = ' xs_src_str '\n\n' ...
    'transl_invar_flag = %g\n'
    ...
    ], T,transl_invar_flag);
fclose(fout);
fprintf(['\nSaved femparams in ' paramsfilename '\n']);

%% save the wave-speed to file:
wavespeed_filename = [save_path '/wave_speed.py'];
fout = fopen(wavespeed_filename, 'w');
fprintf(fout, [...
    'from numpy import *\n' ...
    'def c(x):\n' ...
    '\treturn ' c_code '\n'])
fclose(fout);

end
