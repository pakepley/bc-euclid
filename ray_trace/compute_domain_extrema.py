import os, sys
from mpi4py import MPI
import numpy as np
import scipy.io as sio

## get the data path
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
data_path = sys.argv[1]
sys.path.append(data_path)

## get the ray
from wave_speed        import *
from ray_trace_params  import *
from ray_trace         import *

## set up the communicator
comm = MPI.COMM_WORLD
comm_rank = comm.Get_rank()
comm_size = comm.Get_size()

## xs_rec belongs to femparams
ps_src = np.array([[x, 0.0] for x in xs_src])
nx_src = len(xs_src)

## the control time belongs to femparams
nthetas = 30
nsteps  = 100
nsteps_half = 51

## initialize list to hold the range data
## we will format each entry as follows:
##    (i, xlo[i], xhi[i], yhi[i])
local_range_data = [];

for i in range(comm_rank, nx_src, comm_size):	
	print 'Computing rays for xs_src[{0}]={1}'.format(i,xs_src[i])
	ray_fan = fire_ray_fan(np.array(ps_src[i]), nthetas, nsteps, 2.0 * T)
	xlT,xhT,ylT,yhT = getranges(ray_fan[:nsteps_half,:,:])
	xl2T,xh2T,yl2T,yh2T = getranges(ray_fan)
	local_range_data.append([i, xlT, xhT, yhT, xl2T, xh2T]);

# gather the range data onto process 0
local_range_data = comm.gather(local_range_data, root=0)
comm.Barrier()
if comm_rank == 0:
	# gather all of the local range data into a big list
	range_data = []	
	for k in range(comm_size):
		range_data.extend(local_range_data[k])
	
	# sort the range data by the src index (column 0):
	range_data.sort(key=lambda x: x[0])
	
	# convert to an array:
	range_data = np.array(range_data)
	
	# left, right, top for each source:
	xs_left_T  = range_data[:,1]
	xs_right_T = range_data[:,2]
	ys_top  = range_data[:,3]
	xs_left_2T  = range_data[:,4]
	xs_right_2T = range_data[:,5]

	# compute the largest range required:
	X0 = np.min(xs_left_T)
	X1 = np.max(xs_right_T)
	Y  = np.max(ys_top)

	# extract the domain extrema and save to file
	domain_extrema_mat_file = '{0}/domain_extrema.mat'.format(data_path)
	print 'Saving in file {0}'.format(domain_extrema_mat_file)
	sio.savemat(domain_extrema_mat_file, {'X0': X0,
										  'X1': X1,
										  'Y' :  Y,
										  'xs_left_T' : xs_left_T,
										  'xs_left_2T' : xs_left_2T,
										  'xs_right_T' : xs_right_T,
										  'xs_right_2T' : xs_right_2T,
										  'ys_top' : ys_top})
