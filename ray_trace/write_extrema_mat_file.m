function status = write_extrema_mat_file(save_path, L_src, x_src_separation, ...
                                         T, c_code, transl_invar_flag, n_processors)

    % where this will be saved:
    extrema_mat_file = [save_path '/domain_extrema.mat'];    

    % magic number of points trace rays from:
    xs_src = -L_src : x_src_separation : L_src ;
    save_rtparams(save_path, xs_src, ...
                  T, c_code, transl_invar_flag);
    
    % run the ray tracer:
    ray_trace_dir = [pwd() '/ray_trace'];
    ray_trace_command = ...
        sprintf(['mpirun -n %d python ' ray_trace_dir ...
                 '/compute_domain_extrema.py ' save_path], ...
                n_processors);
    status = system(ray_trace_command);
    
    % Generate a message on error to re-run:
    if status ~= 0
        fprintf('Error running ray trace command.\n');
        fprintf('Try running this at the command line:\n');
        fprintf(ray_trace_command);
    end
        
end