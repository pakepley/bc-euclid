import os, sys
import numpy as np
import scipy.io as sio
from wave_speed import c

## the sound speed c
#def c(x):
#	#return 1.0 + np.exp(-40.0 * ((x[0]*x[0]) + (x[1]-0.5)*(x[1]-0.5)))
#	#return 1.0
#	return 1.0 + 0.2 * exp( - 40.0 * ((x[0]*x[0]) + ((x[1]-.15)*(x[1]-0.15))))

## numerical estimate of the gradient of c
def grad_c(x,dx):
	dim = len(x)
	k = np.zeros(np.size(x))
	grad_c = np.zeros(np.size(x))

	for i in range(0,dim):
		k[i] = dx
		grad_c[i] =  (-c(x+2.0*k) + 8.0*c(x+k) - 8.0*c(x-k) + c(x-2.0*k))/(12. * dx)
		k[i] = 0    
	return grad_c

## Hamiltonian resulting from wave-speed
def HAMIL(x,xi):	
	return 0.5 * (c(x)**2 * np.inner(xi,xi) -1)

# x-gradient of Hamiltonian
def dHAMIL_dxi(x,xi):
	return c(x)**2 * xi

## xi-gradient of Hamiltonian, numerically estimated
def dHAMIL_dx(x,xi,dx):
	dim = len(x)
	k = np.zeros(np.size(x))
	dHAMIL_dx = np.zeros(np.size(x))

	for i in range(0,dim):
		k[i] = dx
		dHAMIL_dx[i] = (-HAMIL(x+2.0*k,xi) + 8.0*HAMIL(x+k,xi) - 8.0*HAMIL(x-k,xi) + HAMIL(x-2.0*k,xi))/(12.0 * dx)		
		k[i] = 0
	return dHAMIL_dx

## norm of covector in metric g = c^{-2}e, so:
##        \|xi\|_g = c(x) \|xi\|_e
def covec_norm_g(x,xi):
	return c(x) * np.sqrt(np.inner(xi,xi))

## normalize a covector
def g_normalize_covec(x,xi):
	return (1.0 / covec_norm_g(x,xi)) * xi


## stopping condition
def out_of_bounds(x):
	return x[1] < 0.0		

# http://en.wikipedia.org/wiki/Eikonal_equation
def solve_hamilton_jacobi(x0,xi0,n,T, return_case=1):
	dim = len(x0)
	xs = np.zeros((n,dim))

	# dx for taking gradients
	dx = .001

	travel_time   = 0.0	
	abs_max_hamil = 0.0	
	travel_time   = 0.0
	
	# the step-size and it's multiples
	h        =  T / float(n)
	half_h   =  0.5 * h
	h_over_6 =  h / 6.0
	
 	# initialize
	x_i  = x0[:]
	xi_i = xi0[:]
		
	# first term in xs
	xs[0,:] = x0

	for i in range(n):		
		# following line is for de-bugging
		if abs(HAMIL(x_i,xi_i)) > abs_max_hamil :
			abs_max_hamil = abs(HAMIL(x_i,xi_i))
			
		travel_time = travel_time + h
		
		kx_1  =  dHAMIL_dxi(x_i,xi_i)
		kxi_1 = -dHAMIL_dx (x_i,xi_i, dx)
		
		kx_2  =  dHAMIL_dxi(x_i + half_h * kx_1, xi_i + half_h * kxi_1)
		kxi_2 = -dHAMIL_dx (x_i + half_h * kx_1, xi_i + half_h * kxi_1, dx)
		
		kx_3  =  dHAMIL_dxi(x_i + half_h * kx_2, xi_i + half_h * kxi_2)
		kxi_3 = -dHAMIL_dx (x_i + half_h * kx_2, xi_i + half_h * kxi_2, dx)
		
		kx_4  =  dHAMIL_dxi(x_i + h * kx_3, xi_i + h * kxi_3)
		kxi_4 = -dHAMIL_dx (x_i + h * kx_3, xi_i + h * kxi_3, dx)
		
		x_i =   x_i + h_over_6 * (kx_1  + 2*kx_2  + 2* kx_3  + kx_4 ) 
		xi_i = xi_i + h_over_6 * (kxi_1 + 2*kxi_2 + 2* kxi_3 + kxi_4)

		xs[i,:] = x_i

		if out_of_bounds(x_i):
			index_of_break = i
			# estimate where the ray pierces the line y = 0 by solving for
			# tau* where:
			#            0 = y_i-1 + tau* (y_i - y_i-1)
			# then the total travel time and x-position should be estimated
			# by:
			#            t* = t_i-1 + tau* (t_i - t_i-1)
			#            x* = x_i-1 + tau* (x_i - x_i-1)

			tau_star = xs[i-1,1] / (xs[i-1,1] - xs[i,1])

			total_travel_time = h * index_of_break + tau_star * h
			x_final           =  xs[i-1,0] + tau_star * (xs[i,0] - xs[i-1,0])

			break

	# if we broke, we went out of bounds, so we'll just set all the 
	# remaining xs as the x_i at the break	
	if 'index_of_break' in locals():		
		for j in range(index_of_break,n):
			xs[j,:] = x_i
	else:
		total_travel_time = travel_time
		x_final           = xs[-1,0]


	if return_case == 1:
		return xs
	elif return_case == 2:
		return x_i, xi_i
	elif return_case == 3:
		return xs, x_i, xi_i
	elif return_case == 4:
		return xs, total_travel_time, x_i, xi_i
	elif return_case == 5:
		return total_travel_time
	elif return_case == 6:
		return total_travel_time, x_i

def getranges(xs):
	xlo = np.min(xs[:,0,:])
	xhi = np.max(xs[:,0,:])
	ylo = np.min(xs[:,1,:])
	yhi = np.max(xs[:,1,:])
	return xlo,xhi,ylo,yhi


def fire_ray_fan(x0, nthetas, n, T, theta_lo = 0.0, theta_hi = np.pi):
	xs = np.zeros((n,2,nthetas+1))
	for i in range(nthetas + 1):
		theta_i = theta_lo + (i * (theta_hi - theta_lo)) / nthetas		
		xi0 =  np.array([np.cos(theta_i), np.sin(theta_i)])
		xi0 =  g_normalize_covec(x0,xi0)		
		xs[:,:,i]  =  solve_hamilton_jacobi(x0,xi0,n,T)				
	return xs
