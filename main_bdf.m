setup_dir_flag         =  true;
run_solver_flag        =  true;
init_inversion_flag    = false;
solve_bc_flag          = false;
compute_distances_flag = false;
plot_distances_flag    = false;

%% Include some helper functions and raytracers
addpath(['./io_facilities']);
addpath(['./compute_bdf']);
addpath(['./ray_trace']);

src_dir = pwd();
user_home = getenv('HOME');

save_base = [user_home '/Computational_Data'];
if not(exist(save_base,'dir'))
    mkdir(save_base);
end

save_root = [save_base '/bcm_data_early_on_gen_src'];
if not(exist(save_root, 'dir'))
    mkdir(save_root);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setup_dir_flag    
    save_path = [save_root '/' timestamp()];    
    mkdir(save_path);
    
    %% Save a snapshot of the source code
    save_source_code(save_path);

    % save parameters and directory
    if not(exist([save_path '/discr.mat'], 'file'))
        data_type          =    'ND';
        start_type         = 'safe';
        x_src_separation   =   0.10;
        x_rec_separation   =   0.05;
        L_src              =    2.0;
        L_rec              =    4.0; 
        t_separation       =   0.10;
        T                  =    1.25;
        n_processors       =      4;
        c_code             =   '1.0';
        %c_code             =   '1 + x[1]';
        transl_invar_flag  =   false;
        domain_type        = 'half_space';
        
        
        addpath(['./' domain_type]);    
        if strcmp(domain_type, 'half_space')
            % discr = init_params(save_path, data_type, start_type, T, ...
            %                     t_separation, L, spatial_separation, c_code, ...
            %                     n_processors, transl_invar_flag);
            discr = init_params(save_path, data_type, ...
                         start_type, T, t_separation, ...
                         L_src, L_rec, ...
                         x_src_separation, x_rec_separation, ...
                         c_code, n_processors, transl_invar_flag)            
        else
            error('Disk case not yet implemented');
        end
        
        save_femparams(discr, save_path);
        discr = basis_x_normalizations(discr,save_path);       
        save([save_path '/discr.mat'], 'discr');
    end
elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');
    load([save_path '/discr.mat']);
    addpath(['./' discr.domain_type]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_solver_flag
  diary([save_path '/fem_diary.txt']);

  if not(exist('discr','var'))
      % get the discr from the save_path location
      load([save_path '/discr.mat']);
  end

  %run_femsolver(discr, save_path, 'all_probe_mode', [save_path '/sol']);
  run_femsolver(discr, save_path, 'trace_probe_mode');
  diary off;
end 


if init_inversion_flag
    if not(exist('discr','var'))
        % get the discr from the save_path location
        load([save_path '/discr.mat']);
    end
        
    discr = init_inversion(discr, save_path);
end


if solve_bc_flag
    if not(exist('discr','var'))
        load([save_path '/discr.mat']);
    end
    discr = init_inversion(discr, save_path);

    y =   .0;
    z =   .5;
    s =   .5;
    r = .125;
    h =  .05;

    %% Inversion parameters:
    Nouter    = 300; 	% maximum number of gmres outer iterations
    Nrestart  =  20; 	% maximum number of gmres inner iterations
    tolerance = 1e-6;   % the GMRES tolerance
    alpha  = 0.00001;	% Tikhonov regularization parameter
    discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, ...
                                                   tolerance);

    % output_root = [save_path '/experiment_example'];
    % conditional_mkdir(output_root);        
    % for i = 1:3
    %     % make the tau
    %     if i == 1
    %         demo = 'disk';
    %         tau = make_tau(discr, 0, y, s + h); %DISK EXAMPLE
    %     elseif i == 2
    %         demo = 'flat';
    %         tau = make_tau(discr, s);           %FLAT EXAMPLE
    %     elseif i == 3
    %         demo = 'flat_and_cap';
    %         tau = make_tau(discr, s, y, h);     %FLAT AND CAP  EXAMPLE
    %     end
    %     restrict the tau?
    %     tau = (-1.25000001 <= discr.xs_src & discr.xs_src <= 1.25000001) .* tau;
    %     output_path = [output_root '/' demo];        
    %     conditional_mkdir(output_path);

    %     % solve the problem
    %     [f, mask] = solve_bc(discr, discr_inversion, ...
    %                          generate_rhs_for_bdf(discr), tau);
    
    %     save([output_path '/data.mat'], 'f', 'mask', 'y','s','h', ...
    %          'discr_inversion');
    % end

    output_root = [save_path '/experiment_example'];
    conditional_mkdir(output_root);        
    for i = 1:4
        % make the tau
        demo = sprintf('tau%d',i);
        if i == 1
            tau = make_tau(discr, s);
        elseif i == 2
            tau = make_tau(discr, s, y, h);
        elseif i == 3
            tau = make_tau(discr, s, z, r);
        elseif i == 4
            tau = make_tau(discr, s, y, h, z, r);
        end
        output_path = [output_root '/' demo];        
        conditional_mkdir(output_path);

        % solve the problem
        [f, mask] = solve_bc(discr, discr_inversion, ...
                             generate_rhs_for_bdf(discr), tau);
    
        save([output_path '/data.mat'], 'f', 'mask', 'y','s','h', 'z', ...
             'r', 'discr_inversion');
    end                
end


if compute_distances_flag
    if not(exist('discr','var'))
        load([save_path '/discr.mat']);
    end    
    discr = init_inversion(discr, save_path);

    distance_root = [save_path '/distances'];
    if not(exist(distance_root,'dir'))
        mkdir(distance_root);
    end
       
    % What point x(y,s) to compute distances to?
    y = 0.0;
    s_desired = .5;
    s = discr.t_separation * ceil(s_desired / discr.t_separation) ;
    h = 2 * discr.t_separation;
    
    %% Inversion parameters:
    Nouter    = 300; 	% maximum number of gmres outer iterations
    Nrestart  =  20; 	% maximum number of gmres inner iterations
    tolerance = 1e-6;   % the GMRES tolerance
    alpha  = 0.00001;	% Tikhonov regularization parameter
    discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, ...
                                                   tolerance);
    
    %% Where we will save everything
    distance_path = [distance_root ...
                     sprintf(['/experiment_y_%.03e_s_%.03e_h_%.03e'], ...
                             y,s,h)];

    if not(exist(distance_path,'dir'))
        mkdir(distance_path);
    end
    save([distance_path '/discr_inversion.mat'], 'discr_inversion');
        
    j_lo = discr.mx_src ;
    j_hi = find(discr.xs_src >= .99, 1, 'first') - 1;
    stride = 4;
    mode = 'minimal' ;

    fprintf('Will compute distances between x(%.04f,%.04f) and zs(%d:%d:%d)\n', ...
            y, s, j_lo, stride, j_hi);

    generate_data_for_distances(discr, discr_inversion, distance_path, ...
                                s,y,h, ...
                                j_lo, j_hi, stride, mode);

    compute_distances(discr, s, distance_path);

    if plot_distances_flag
        make_distance_images(discr, save_path, distance_path);    
    end
end
