function [xs_sol, ys_sol, nxlnew, nxrnew] = get_basic_sol_padding(o)
%% --------------------------------------------------------------------
%% Function: get_basic_sol_padding(o)
%% Use:      get padding for basic solutions
%% --------------------------------------------------------------------
%% Inputs:
%% o         - the bcm discretization structure
%% --------------------------------------------------------------------
%% Outputs:
%% xs_sol    - the zero padded coordinates
%% ys_sol    - the zero padded coordinates
%% nxlnew    - number of new solution new sol positions added on left
%% nxrnew    - number of new solution new sol positions added on right
%% --------------------------------------------------------------------
    
    %% The soln was recorded on o.xs_sol, but this may not be enough points
    if abs(o.xs_sol(1) - o.xs_rec(1)) < .1 * o.sol_res
        %% then we only recorded in \Gamma x [0, T]
        nxlnew = ceil((o.xs_sol(1) - o.X0) / o.sol_res) ;
        nxrnew = ceil((o.X1 - o.xs_sol(end)) / o.sol_res) ;
        xs_lo  = o.xs_sol(1)   - nxlnew * o.sol_res;
        xs_hi  = o.xs_sol(end) + nxrnew * o.sol_res;
        xs_sol = xs_lo : o.sol_res : xs_hi;
    else
        %% then we only recorded in \Gamma x [0, T]
        nxlnew = 0;
        nxrnew = 0;
        warning('double check that xs_sol is what you want!');
        xs_sol = o.xs_sol;
    end

    ys_sol = o.ys_sol;
    
end