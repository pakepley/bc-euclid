function o = init_params(save_path, data_type, ...
                         start_type, T, t_separation, ...
                         L_src, L_rec, ...
                         x_src_separation, x_rec_separation, ...
                         c_code, n_processors, transl_invar_flag, y_in)
%% --------------------------------------------------------------------
%% NOTE: HALF SPACE VERSION OF INIT_PARAMS
%% --------------------------------------------------------------------
%% Function: init_params(save_path, data_type, start_type, T,
%%                       t_separation, L, x_separation,
%%                       c_code, n_processors, transl_invar_flag, 
%%                       y_in)
%% Use:      set up the discretization for the BCM problem
%% --------------------------------------------------------------------
%% Inputs:
%% save_path     - where everything is saved
%% data_type     - whether this is a discretization for interior or
%%                 boundary data
%% start_type    - either this will be 'early' or 'safe'. If it's
%%                 early, then ts_src(1) = o.ts_src, If it's safe
%%                 then ts_src(1) = o.wt / 2.
%% T             - the control time
%% t_separation  - the time between source applications
%% L             - the width of the control set
%% x_separation  - the distance between the source centers in space
%%                 this should be a number of the form 2pi/n, if not
%%                 it will be converted to one.
%% c_code        - the string of code that defines the wavespeed c,
%%                 note that this should be valid C code.
%% n_processors  - how many processors are available for running the 
%%                 finite element solver
%% transl_invar_flag - a flag to save time and energy if we know that
%%                     the wave-speed does not depend upon x
%% --------------------------------------------------------------------
%% Optional Inputs:
%% y_in          - force the domain to have a specific height
%% --------------------------------------------------------------------
%% Outputs:
%% o             - the bcm discretization
%% --------------------------------------------------------------------

o = struct();

% Keep track of whether we are in the disk or half-space case
o.domain_type = 'half_space';

% save the data type
o.data_type = data_type ;

% should ts_src(1) start early? or should we be conservative and start
% ts_src(1) later?
o.start_type = start_type;

% Numerical tolerance. Ie, we say the sources are zero when they fall below this threshold.
% Plays a role in determining the width of our pulses.
o.epsilon = 10^(-6);

% Parameter to be used by Gaussian in space
o.ratio_x_sep_t_sep = 1.0;
o.x_src_separation = x_src_separation;
o.x_rec_separation = x_rec_separation;

% Parameter to be used by Gaussian in time
o.t_separation = t_separation;
o.T = ceil(T / o.t_separation) * o.t_separation;

% Our pulses will need to overlap some. We quantify this by giving a ratio between the
% halfwidth of the pulse and the separation between pulse peaks. So that we can delay the 
% pulses correctly, this should be a rational number in (0,2]. Moreover, the numerator
% and denominator should be small numbers when in lowest terms. I would imagine that
% the best values should be between (0,1]
o.ratio_x_separation_hwx = 1/4;
o.ratio_t_separation_hwt = 1/4;
o.wx = (2.0 * o.x_src_separation) / o.ratio_x_separation_hwx
o.wt = (2.0 * o.t_separation) / o.ratio_t_separation_hwt

% The numerical support of our Pulses:
fprintf([ ...
    'Spatial width of the pulse %g\n' ...
    'Temporal width of the pulse %g\n' ...
     ], o.wx, o.wt);


% receiver information
o.ratio_x_separation_dx = 2;                           % SHOULD BE AN INTEGER
o.dx = o.x_rec_separation;
o.ratiowxdx = o.wx/o.dx;                               % SHOULD BE AN INTEGER CHECK!


% Parameters to be used by Gaussian in space. 
% ie, in space, f(x) = exp(-ax * (x-xc).^2) 
o.ax = - (log(o.epsilon) / ( (4 * o.x_src_separation))^2) ;
o.at = - (log(o.epsilon) / ( (4 * o.t_separation))^2) ;

% Source normalization parameters. They depend upon the source. If the
% source is changed, then these need to be changed!!!!!
o.normalization_basis_t = ((2*o.at)/pi)^.25;
%% OLD: o.normalization_basis_x = ((2*o.ax)/pi)^.25;
%% OLD: o.normalization_Lambda = o.normalization_basis_x * o.normalization_basis_t;

% L_src is the set we place sources in, L_rec is where we put receivers
o.L_src = ceil(L_src / o.x_src_separation) * o.x_src_separation ;
o.L_rec = ceil(L_rec / o.x_rec_separation) * o.x_rec_separation ;

% points where N-to-D map is evaluated:
o.xs_src = - o.L_src : o.x_src_separation : o.L_src;
o.nx_src = length(o.xs_src);
o.mx_src = ceil(length(o.xs_src) / 2);

if strcmp(o.start_type, 'safe')
    o.ts_src = (o.wt/2):o.t_separation:(o.T-(o.wt/2));
elseif strcmp(o.start_type, 'early')             
    o.ts_src = o.t_separation:o.t_separation:(o.T-o.t_separation);
else
    error('check the start_type');
end
o.nt_src = length(o.ts_src);
o.mt_src = ceil(length(o.ts_src) / 2);

fprintf([ ...
    'The connecting operator K is discretized using\n' ...
    ' %g src positions x in [-L/2,L/2] and %g source times t in [0, T) \n' ...
    ' where L = %g and T = %g\n' ...
    ], o.nx_src, o.nt_src, o.L_src, o.T);


% We give a heuristic estimate of the spectral content of the source
% ie, look at the fourier transform of a Gaussian and find where 
% the Gaussian falls to spectralEpsilon
o.spectralEpsilon = 10^(-8);
o.nuMax = sqrt(- (o.at/pi^2) * log( o.spectralEpsilon * sqrt(o.at/pi)));

% We now use the nuMax estimate to figure out how small our time step should be 
% Note: this will need to be changed in variable sound speed media!
o.min_points_per_wave = 8;
[N,D] = rat(o.ratio_t_separation_hwt);

% Start with pointsPerWaveTMP too big
i = 0;
pointsPerWaveTMP = o.min_points_per_wave + 1;
while(pointsPerWaveTMP > o.min_points_per_wave && i <= o.min_points_per_wave)
    kTMP = ceil((o.wt/2) * (o.min_points_per_wave - i) * o.nuMax); %An integral multiplier 
    dtTMP = o.wt/(kTMP * 2 * D);
pointsPerWaveTMP = 1/(dtTMP * o.nuMax);
    if( pointsPerWaveTMP >= o.min_points_per_wave)
        k = kTMP;
    end
    i = i + 1 ;
end

% Compute the time step and related parameters
o.ratiowtdt = 2 * k * D;
o.ratiohwtdt = k * D;
o.ratio_t_separation_dt = k * N;
o.dt = o.wt/o.ratiowtdt;

% We tell the user how many points per wave are actually being used
o.actual_points_per_wave = 1/(o.dt * o.nuMax);


% margin for the fem solver
o.ratio_t_margin_dt = 2 * ((1 / o.ratio_t_separation_hwt) - 1);
%o.ratio_t_margin_dt = ((1 / o.ratio_t_separation_hwt) - 1);
o.nt_margin = o.ratio_t_margin_dt * o.ratio_t_separation_dt;

o.ts_rec = 0:o.dt:2*o.T;
o.nt_rec = length(o.ts_rec);
o.mt_rec = ceil(length(o.ts_rec)/2);
fprintf([ ...
    'The measurement is discretized using\n' ...
    ' %g points t in [0, 2T] where 2T = %g\n' ...
	  ], length(o.ts_rec), 2*o.T);


% We need more receiver points than source points. These should be collected
% at points which fit evenly into x_separation. So 
o.xs_rec = -o.L_rec : o.x_rec_separation : o.L_rec;
o.nx_rec = length(o.xs_rec);


%% The following are used by the solver 
o.n_processors = n_processors;  
fprintf(['The femsolver will run with: %d processors.\n'], n_processors);

% include a buffer 
o.xmargin = 2.0 * o.wx;    %TODO what is a good value here?

%% c_code
o.c_code = c_code;

%% is the domain translation invariant or not
o.transl_invar_flag = transl_invar_flag;

% if we haven't pre-written the extrema mat file, write it.
% once it exists, we load it.
extrema_mat_file = [save_path '/domain_extrema.mat'];
if not(exist(extrema_mat_file, 'file'))
    fprintf('writing extrema file');    
    write_extrema_mat_file(save_path, L_src, x_src_separation, ...
                           T, c_code, transl_invar_flag, n_processors);
end
load(extrema_mat_file);

%% height of the domain:
if exist('y_in','var')
    warning(['User supplied y_in. Using this instead of Y from raytracing. ' ...
             'The file ' save_path '/domain_extrema.mat will be ' ...
             'modified.']);
    Y_from_raytracing = Y;
    Y = y_in;
    save(extrema_mat_file, 'Y', 'Y_from_raytracing', '-append');
end
o.Y  =                 Y; 
o.X0 = min(X0, -o.L_rec);
o.X1 = max(X1,  o.L_rec);

%% The following sets up the default sol probe locations.

% solution plotting resolution?
o.sol_res = o.x_src_separation ;

% specify y-coordinates for low-resolution plots. 
o.sol_y_lo = 0.0;
o.sol_y_hi = o.Y;
o.ys_sol = o.sol_y_lo : o.sol_res : o.sol_y_hi;
o.ny_sol = length(o.ys_sol);

% specify x-coordinates for low-resolution plots. Note that I'm currently
% only saving points with x-coordinates in Gamma. This should probably
% be changed
o.sol_x_lo = o.xs_rec(1)  ;
o.sol_x_hi = o.xs_rec(end);
o.xs_sol   = o.sol_x_lo : o.sol_res : o.sol_x_hi;
o.nx_sol   = length(o.xs_sol);

% save the femparams
save_femparams(o, save_path);

% compute the normalizations for the basis functions
o = basis_x_normalizations(o);

end
