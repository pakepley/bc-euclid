function F = interior_source(F_params,t,x,y)
    ax = F_params.ax;
    at = F_params.at;
    xc = F_params.xc;
    yc = F_params.yc;
    tc = F_params.tc;
    
    %% in this case, x and y are assumed to be computed with a meshgrid
    %% and t is probably just a 1 
    if and(size(x) == size(y), not(size(t) == size(x)))             
        space_dims = size(x);

        %% Gurantee that t is a vector with size nt x 1
        t = (t(:)')';
        Nt = length(t);
        F_t  = exp(-at*(t-tc).^2);                
        
        %% Evaluate the spatial part of the source and flatten into a 
        %% 1 x (nx * ny) matrix
        NN = size(x);
        N1 = NN(1);
        N2 = NN(2);
        F_xy = exp(-ax*((x-xc).^2 + (y-yc).^2));         
        F_xy = F_xy(:)';        
        
        %% Compute F, F = F_t * F_xy is an nt x (nx * ny) array,
        %% which we then rearrange
        F = F_t * F_xy;
        F = reshape(F, Nt, N1, N2);
    end
end