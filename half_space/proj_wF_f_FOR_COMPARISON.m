function wF_proj = proj_wF_f(o, output_path)
%% blah
%%
    
    
    warning('THIS FUNCTION IS ONLY FOR TESTING PURPOSES');
    
    %% get the trace for wF, projected
    wF_trace   = dlmread([output_path '/trace.txt']); 
    size(wF_trace)
    wF_proj = proj_basis(o,wF_trace);
    wF_proj = reshape(wF_proj, o.nt_src, o.nx_src);
    
end