function windowed_uf_ij = get_windowed_uf_ij(o,uf_i_1, i, j, xinds, yinds)
%% --------------------------------------------------------------------
%% Function: get_windowed_uf_ij(uf_i_1, i, j, rows, cols)
%% Use:      use the solution uf_i_1 in such a way that we obtain
%%           uf_i_j spatially windowed into the xs and ys
%%           that correspond to rows and cols
%% --------------------------------------------------------------------
%% Inputs:
%% uf_i_1   - the full wave uf with f = f_{i,1}, ie centered at
%%             xs_src(i) in space and ts_src(1) in time
%% i         - the basis function, f_ij's spatial index, i.e. so that
%%             f_{ij} is centered on xs_src(i) spatially
%% j         - the basis function, f_ij's temporal index i.e. so that
%%             f_{ij} is centered on ts_src(j) in time
%% xinds     - the x-indices of uf_ij we want to keep
%% yinds     - the y-indices of uf_ij we want to keep
%% --------------------------------------------------------------------
%% Outputs:
%% windowed_uf_ij - the windowed version of uf_ij
%% --------------------------------------------------------------------
    
%% Get the basic solution which we will translate and delay
tmp =  size(uf_i_1);
Nt  =        tmp(1);
Nx  =        tmp(2);
Ny  =        tmp(3);

%% how big must the windowed output be?
nts  = tmp(1);
nxs  = length(xinds);
nys  = length(yinds);

%% we can't slice to negative inds or greater than nxs inds!
xlo    = min(find(xinds >= 1));
xhi    = max(find(xinds <= Nx));

%% how much zero padding to delay in time?
delay = o.ratio_t_separation_dt * (j - 1);

%% compute the windowed uf_ij
windowed_uf_ij = zeros(nts, nxs, nys);
windowed_uf_ij(delay+1:end, xlo:xhi, :) = uf_i_1(1 : nts - delay, xinds(xlo:xhi), yinds);

end

