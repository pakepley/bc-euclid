import sys, os
import numpy as np
import scipy.io as sio

# get the simulation save_path:
data_path = sys.argv[1]
sys.path.append(data_path)

print sys.path

# grab the simulation parameters and wave_speed:
from femparams import *
from wave_speed import *

# the values of c at the quadrature nodes
c_at_xs_rec = np.zeros(len(xs_rec))

for i in range(len(xs_rec)):
	c_at_xs_rec[i] = c([xs_rec[i], 0.0])

# save the values of c evaluated on the xs_rec as a row vector
sio.savemat('{0}/c_at_xs_rec.mat'.format(save_path), 
			{'c_at_xs_rec':c_at_xs_rec}, 
			oned_as='row')


c_at_ps_sol = np.zeros((len(xs_sol), len(ys_sol)))

for i in range(len(xs_sol)):
	for j in range(len(ys_sol)):
		c_at_ps_sol[i,j] = c([xs_sol[i], ys_sol[j]])

# save the values of c evaluated on the (xs_sol,ys_sol)
sio.savemat('{0}/c_at_ps_sol.mat'.format(save_path), 
			{'c_at_ps_sol':c_at_ps_sol})
