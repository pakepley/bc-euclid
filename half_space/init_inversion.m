function o = init_inversion(o, save_path, matrix_path)
%% --------------------------------------------------------------------
%% Function: init_inversion(o, save_path)
%% Use:      compute the jth spatial basis function
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% save_path  - where the data was saved
%% --------------------------------------------------------------------
%% Optional Inputs:
%% matrix_path - where the matrices are saved. The default is
%%                     save_path/matrices
%% --------------------------------------------------------------------
%% Outputs:
%% o          - the bcm discretization structure, updated with the 
%%              matrix coeffs_K
%% --------------------------------------------------------------------

Lambda = read_femtrace(o, save_path);

% normalizations, add to the params
normalization_Lambda = o.normalization_Lambda;

% shorthand notation read from the params
nt_src = o.nt_src;
nt_rec = o.nt_rec;
nx_src = o.nx_src;
nx_rec = o.nx_rec;
mt_rec = o.mt_rec;
n_basis = nt_src * nx_src;
assert(abs(o.ts_rec(mt_rec) - o.T) <= eps);
ratio_t_separation_dt = o.ratio_t_separation_dt;
dt = o.dt;

if not(exist('matrix_path', 'var'))
    matrix_path = [save_path '/matrices'];
end

if isfield(o, 'coeffs_K') && isfield(o, 'coeffs_G')
    disp('Matrices have been pre-loaded');
else
    try
        fprintf('Looking for pre-computed matrices. Loading if present.\n');

        % we already computed everything. just load what we need.
        load([matrix_path '/G.mat']);
        o.coeffs_G = coeffs_G;
        load([matrix_path '/K.mat']);
        o.coeffs_K = coeffs_K;
        fprintf('Matrices loaded\n');

    catch
        % grid to evaluate functions on
        [tmpXs, tmpTs] = meshgrid(o.xs_rec, o.ts_rec(1:mt_rec));
        
        if not(exist(matrix_path, 'dir'))
            mkdir(matrix_path);
        end

        try
            matlabpool('local',12);
        end

        if not(exist([matrix_path '/G.mat']))
            disp('Building Gram matrix, G');
            coeffs_G = zeros(nt_src*nx_src);
            for i=1:nx_src
                progress(i,nx_src);
                parfor j = 1:nt_src
                                        
                    tmpCoeffBM{j} =  proj_basis(o, ...
                                                fun_basis_t(o, tmpTs, j) .* ...
                                                fun_basis_x(o, tmpXs, i));
                end
                for j = 1:nt_src
                    coeffs_G(:, (i-1)*nt_src + j) = tmpCoeffBM{j};
                end
            end
            clear('tmpCoeffBM');
            coeffs_G = sparse(coeffs_G);
            if not(running_octave())
                save([matrix_path '/G.mat'], 'coeffs_G', '-v7.3');
            else
                save([matrix_path '/G.mat'], 'coeffs_G', '-mat7-binary');
            end
        end

        if not(exist([matrix_path '/invG.mat']))
            try
                matlabpool('close');
            end

            disp('Computing inverse of G, invG');

            coeffs_invG = coeffs_G^(-1);
            if not(running_octave())            
                save([matrix_path '/invG.mat'],'coeffs_invG','-v7.3');
            else
                save([matrix_path '/invG.mat'],'coeffs_invG','-mat7-binary');
            end
            
            clear('coeffs_G', 'coeffs_invG');

            try
                matlabpool('local',12);
            end

        end

        if not(exist([matrix_path '/RJ.mat']))
            disp('Building matrix for R J');
            coeffs_RJ = zeros(nt_src*nx_src);
            for i=1:nx_src
                progress(i,nx_src);
                parfor j=1:nt_src
                    tmpCoeffRJ{j} = proj_basis(o, fun_basis_t_RJ(o, tmpTs, j) ...
                                               .* fun_basis_x(o, tmpXs, i));
                end
                for j = 1:nt_src
                    coeffs_RJ(:, (i-1)*nt_src + j) = tmpCoeffRJ{j};
                end
            end
            clear('tmpCoeffRJ');
            coeffs_RJ = sparse(coeffs_RJ);
            RJ_not_multiplied_by_invG = true;
            %% save to the workspace
            if not(running_octave())
                save([matrix_path '/RJ.mat'], 'coeffs_RJ', ...
                     'RJ_not_multiplied_by_invG', '-v7.3');
            else
                save([matrix_path '/RJ.mat'], 'coeffs_RJ', ...
                     'RJ_not_multiplied_by_invG', '-mat7-binary');
            end
            clear('coeffs_RJ');
        end
        
        
        if not(exist([matrix_path '/RL.mat']))
            disp('Building matrix for R Lambda');
            coeffs_RL = zeros(nt_src*nx_src);
            for i=1:nx_src
                progress(i,nx_src);
                Lambda_i = Lambda(:,:,i);
                parfor j=1:nt_src
                    transl = ratio_t_separation_dt*(j - 1);
                    tmp = zeros(mt_rec, nx_rec);
                    tmp(transl+1:end, :) = normalization_Lambda(i) * Lambda_i(1:mt_rec-transl, :);
                    tmpCoeffRL{j} = proj_basis(o,flipud(tmp));
                end
                for j = 1:nt_src
                    coeffs_RL(:, (i-1)*nt_src + j) = tmpCoeffRL{j};
                end
            end
            clear('tmpCoeffRL');
            coeffs_RL = sparse(coeffs_RL);
            if not(running_octave())
                save([matrix_path '/RL.mat'], 'coeffs_RL', '-v7.3');
            else
                save([matrix_path '/RL.mat'], 'coeffs_RL','-mat7-binary');
            end
            clear('coeffs_RL');
        end


        % if not(exist([matrix_path '/JL.mat']))
        %     disp('Building matrix for J Lambda');
        %     coeffs_JL = zeros(nt_src*nx_src);
        %     for i=1:nx_src
        %         progress(i,nx_src);
        %         Lambda_i = Lambda(:,:,i);
        %         parfor j=1:nt_src
        %             transl = ratio_t_separation_dt*(j - 1);
        %             %L = zeros(size(Lambda, 1), size(Lambda, 2));
        %             L = zeros(nt_rec,nx_rec);
        %             L(transl+1:end, :) = normalization_Lambda * Lambda_i(1:end-transl, :);
        %             % integrals from t to T
        %             tmp1 = flipud(cumsum(flipud(L(1:mt_rec, :))));
        %             % integrals from T to 2T-t
        %             tmp2 = flipud(cumsum(L(mt_rec:end, :)));
        %             tmp = tmp1 + tmp2;
        %             tmpCoeffJL{j} = 0.5 * dt * proj_basis(o, tmp);
        %         end
        %         for j = 1:nt_src
        %             coeffs_JL(:, (i-1)*nt_src + j) = tmpCoeffJL{j};
        %         end
        %     end
        %     clear('tmpCoeffJL');
        %     clear('Lambda');
        %     clear('L');
        %     clear('Lambda_i');
        
        %     if not(running_octave())
        %         save([matrix_path '/JL.mat'], 'coeffs_JL','-v7.3');
        %     else
        %         save([matrix_path '/JL.mat'], 'coeffs_JL','-mat7-binary');
        %     end

        %     clear('coeffs_JL');
        % end

        % try
        %     matlabpool('close');
        % end

        
        if not(exist([matrix_path '/JL.mat']))
            disp('Building matrix for J Lambda');
            coeffs_JL = zeros(nt_src*nx_src);
            for i=1:nx_src
                progress(i,nx_src);
                Lambda_i = Lambda(:,:,i);
                parfor j=1:nt_src
                    transl = ratio_t_separation_dt*(j - 1);
                    %L = zeros(size(Lambda, 1), size(Lambda, 2));
                    L = zeros(nt_rec,nx_rec);
                    L(transl+1:end, :) = normalization_Lambda(i) * Lambda_i(1:end-transl, :);
                    % integrals from t to T
                    tmp1 = flipud(cumsum(flipud(L(1:mt_rec-1, :))));
                    % integrals from T to 2T-t
                    tmp2 = flipud(cumsum(L(mt_rec+1:end, :)));
                    tmp = tmp1 + tmp2 + repmat( L(mt_rec,:), mt_rec - 1, 1) ;
                    tmp(mt_rec, :) = zeros(1, nx_rec);
                    
                    tmpCoeffJL{j} = 0.5 * dt * proj_basis(o, tmp);
                end
                for j = 1:nt_src
                    coeffs_JL(:, (i-1)*nt_src + j) = tmpCoeffJL{j};
                end
            end
            clear('tmpCoeffJL');
            clear('Lambda');
            clear('L');
            clear('Lambda_i');
            
            if not(running_octave())
                save([matrix_path '/JL.mat'], 'coeffs_JL','-v7.3');
            else
                save([matrix_path '/JL.mat'], 'coeffs_JL','-mat7-binary');
            end

            clear('coeffs_JL');
        end

        try
            matlabpool('close');
        end
        
        
        load([matrix_path '/RJ.mat'], ...
             'RJ_not_multiplied_by_invG', 'coeffs_RJ');

        if RJ_not_multiplied_by_invG
            
            disp('Computing G^-1 * RJ');

            load([matrix_path '/invG.mat']);

            %% Promote to full before multiplying
            coeffs_invG = full(coeffs_invG);

            disp(' -Performing multiplication');
            coeffs_RJ = coeffs_invG * coeffs_RJ;
            RJ_not_multiplied_by_invG = false;

            disp(' -Saving to file');
            if not(running_octave())
                save([matrix_path '/RJ.mat'], ...
                     'RJ_not_multiplied_by_invG', 'coeffs_RJ','-v7.3');
            else
                save([matrix_path '/RJ.mat'], ...
                     'RJ_not_multiplied_by_invG', 'coeffs_RJ','-mat7-binary');            
            end
            clear('coeffs_invG');
        end

        if not(exist([matrix_path '/RLRJ.mat']))
            disp('Computing R Lambda R J');

            load([matrix_path '/RL.mat']);        
            if not(exist('coeffs_RJ','var'))            
                load([matrix_path '/RJ.mat']);
            end
            
            %% cast to full to 
            coeffs_RL = full(coeffs_RL);
            coeffs_RJ = full(coeffs_RJ);

            %% multiply
            disp(' -Performing multiplication');
            coeffs_RLRJ = coeffs_RL * coeffs_RJ;

            disp(' -Saving to file');
            if not(running_octave())
                save([matrix_path '/RLRJ.mat'], 'coeffs_RLRJ','-v7.3');
            else
                save([matrix_path '/RLRJ.mat'], 'coeffs_RLRJ','-mat7-binary');
            end
            clear('coeffs_RL', 'coeffs_RJ');
        end

        if not(exist([matrix_path '/K.mat']))
            disp('Computing K');
            load([matrix_path '/JL.mat']);
            load([matrix_path '/RLRJ.mat']);
            
            % the sign differs in N-D and D-N cases
            coeffs_K = coeffs_JL - coeffs_RLRJ ;
            
            if not(running_octave())
                %% we are on matlab and can use the -v7.3 flag
                save([matrix_path '/K.mat'], 'coeffs_K', '-v7.3');
            else
                %% we use matlab -v7 flag
                save([matrix_path '/K.mat'], 'coeffs_K', '-mat7-binary');
            end
            
            clear('coeffs_JL','coeffs_RLRJ');
        end   

        % if not(exist([matrix_path '/invGK.mat']))
        %     disp('Computing G^-1 * K');
        %     disp('Loading K');
        %     load([matrix_path '/K.mat']);
        %     disp('Loading G^-1');
        %     load([matrix_path '/invG.mat']);
            
        %     disp(' -Performing multiplication');
        %     coeffs_invG = full(coeffs_invG);
        %     coeffs_invGK = coeffs_invG * coeffs_K ;
            
        %     disp(' -Saving to file');
        %     if not(running_octave())
        %         %% we are on matlab and can use the -v7.3 flag
        %         save([matrix_path '/invGK.mat'], 'coeffs_invGK', '-v7.3');
        %     else
        %         %% we use matlab -v7 flag
        %         save([matrix_path '/invGK.mat'], 'coeffs_invGK', '-mat7-binary');
        %     end            
        %     clear('coeffs_K');
        % end   
        %
        % o.coeffs_invGK = coeffs_invGK;
        % o.coeffs_invG = coeffs_invG;
        
        o.coeffs_K = coeffs_K;
        
        if not(exist('coeffs_G', 'var'))
            load([matrix_path '/G.mat']);
        end
        o.coeffs_G = coeffs_G;
        
    end
end
end