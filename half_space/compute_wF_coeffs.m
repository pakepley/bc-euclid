function wF_coeffs = compute_wF_coeffs(o, proj_along_RF)
    
    %% we have that (Rw^F, f) = (RF, u^f), and we know the RHS of
    %% this expression.  so we can compute the coefficients of
    %% Rw^F by solving GG h = (RF, u^f)        
    RwF_coeffs = o.coeffs_invG * proj_along_RF(:);
    
    %% now we use that R(phi_{i,j}) = phi_{i,nt-(j-1)} to get the
    %% coefficients of wF
    wF_coeffs = reshape(RwF_coeffs, o.nt_src, o.nx_src);
    wF_coeffs = flipud(wF_coeffs);    
end
