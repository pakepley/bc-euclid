function f = read_coeffs(o, coeff_path)   
%% --------------------------------------------------------------------
%% Function: read_coeffs(o, coeff_path)
%% Use:      read coefficients used in the wave solver. Note, the
%%           wave solver does NOT use normalized basis functions,
%%           so the coefficients read here were pre-multiplied by the
%%           basis normalization. We have to undo that here.
%% --------------------------------------------------------------------
%% Inputs:
%% o           - the bcm discretization structure
%% coeff_path  - the file path for the coefficient file
%% --------------------------------------------------------------------
%% Outputs:
%% f           - the coefficient vector
%% --------------------------------------------------------------------

    nt_src = o.nt_src;
    nx_src = o.nx_src;

    coeffs = dlmread(coeff_path);

    f = zeros(nt_src, nx_src);

    for l = 1:length(coeffs)
        i = coeffs(l, 2);
        j = coeffs(l, 1);
        f(i,j) = coeffs(l,3);

        %% the wave solver does not use normalized basis functions, so
        %% the coefficients saved by save_coeffs must be pre-multiplied
        %% by o.normalization_Lambda. We get rid of that here
        f(i,j) = f(i,j) / o.normalization_Lambda(i);
    end

end