import numpy as np
import os, glob

def consolidateProbes(save_path, sub_dir, probe_name, n_probes, nt_meas):
	probepath = save_path + '/' + sub_dir
	the_probe_array = []
	
	# Read the probes from file into memory
	for i in range(0,n_probes):
		the_probe_file_name = probepath + '/' + probe_name + '_{0}.probe'.format(i)
		if os.path.exists(the_probe_file_name):
			the_probe_file = open(the_probe_file_name,'r')
			the_probe = list(the_probe_file)
			the_probe_file.close()
			
			# for some reason, some probe files get too much written on them!
			meas_expected = int(float(the_probe[1].split(' ').pop()[:-1])) # tells how many on first line
			the_probe = the_probe[5:5+meas_expected]                       # first 5 lines are junk
			the_probe = map(lambda x: float(x[:-2]), the_probe)            # strip off string formatting
		else:
			the_probe = list(np.zeros(nt_meas+1))
		the_probe_array.append(the_probe)
			
	the_probe_file = open(save_path + '/' + sub_dir + '.txt','w')
	for i in range(nt_meas):
		for probe in the_probe_array:
			the_probe_file.write(repr(probe[i])+' ')
		the_probe_file.write('\n')
	the_probe_file.close()

	# clean it up a bit
	os.system('tar -zcf {0}/{1}.tar.gz -C {0} ./{1}'.format(save_path, sub_dir))
	os.system('rm -rf {0}/{1}'.format(save_path, sub_dir))
	archive_path = '{0}/archive'.format(save_path)
	if not(os.path.exists(archive_path)):
		os.makedirs(archive_path)
	os.system('mv {0}/{1}.tar.gz {2}'.format(save_path, sub_dir, archive_path))
