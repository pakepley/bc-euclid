function rhs_integrals = compute_int_src_rhs(o, oi, proj_along_RF, proj_along_JF)
    
    %% compute the coefficients of the approximation to wF in our basis
    wF_coeffs = compute_wF_coeffs(o, proj_along_RF);
        
    %% compute the approximation, wF_fun, to the dirichlet trace of wF as a
    %% function on [0,T] \times \Gamma
    [Xs,Ts] = meshgrid(o.xs_rec, o.ts_rec(1:o.mt_rec));
    wF_fun = zeros(o.mt_rec, o.nx_rec);
    for k=1:o.nx_src
        phi_xk = fun_basis_x(o, Xs, k);
        for j=1:o.nt_src    
            phi_tj = fun_basis_t(o, Ts, j);
            wF_fun = wF_fun + wF_coeffs(j, k) * phi_xk .* phi_tj;
        end
    end
            
    %% compute the approximation JwF_fun to J^T(wF) as a function on 
    %% [0,T/2] \times \Gamma:    
    %% first we need some things for the integrals:
    dt         =     oi.dt ;
    mt_rec_T_2 = oi.mt_rec ;
    %% integral from t to T/2
    tmp1 =  dt * flipud(cumsum(flipud(wF_fun(1 : mt_rec_T_2,   :)))) ;    
    %% integral from T/2 to T-t
    tmp2 =  dt * flipud(cumsum(       wF_fun(mt_rec_T_2 : end, :)))  ;    
    %% JwF(t) = 0.5 \int_t^{T/2 - t} F(s) ds 
    JwF_fun   = 0.5 * (tmp1 + tmp2);    
    
    %% we project JwF onto the basis for [0,T/2] \times \Gamma
    JwF_integrals = proj_basis(oi, JwF_fun);    
    
    %% the integrals on the right hand side of the control problem are 
    %% given by:
    rhs_integrals = JwF_integrals(:) - proj_along_JF(:);

end
