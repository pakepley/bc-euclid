function plot_sol_from_ut(o, ut, output_path, file_name, color_axis, ...
                          axis_vector, font_size)

[xs_sol, ys_sol, nxlnew, nxrnew] = get_basic_sol_padding(o)
[Xs,Ys] = meshgrid(xs_sol,ys_sol);
Xs = Xs';
Ys = -Ys';

if display_attached()    
    %% matlab can generate nice images if a display is not attached
    surf(Xs, Ys, ut);
    shading interp;
    view(0, 90);
    axis equal;
    axis tight;    
    colorbar;
    if exist('color_axis', 'var')
        caxis(color_axis);
    end
    if exist('axis_vector', 'var')
        axis(axis_vector);
    end
    if exist('font_size', 'var')
        set(gca,'FontSize', font_size);
    end
    if and(exist('output_path', 'var'), exist('file_name', 'var'))
        print('-dpng', [output_path '/' file_name '.png']);
    end
else
    %% matlab cannot generate nice images if a display is not attached
    imagesc(flipud(ut'));
    axis equal;
    axis tight;    
    colorbar;
    if exist('color_axis', 'var')
        caxis(color_axis);
    end
    if and(exist('output_path', 'var'), exist('file_name', 'var'))
        print('-dpng',[output_path '/' file_name '.png']);
    end
end

end

