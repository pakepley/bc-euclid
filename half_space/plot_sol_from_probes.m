function ut = plot_sol_from_probes(o, basic_sol, f, t, output_path, ...
                              file_name, color_axis)
%% --------------------------------------------------------------------
%% Function: plot_sol_from_probes(o, basic_sol, f, t, output_path, ...
%%                                file_name, color_axis)
%% Use:      plot sol from probe measurements. assumes that the sound
%%           speed is translation invariant
%% --------------------------------------------------------------------
%% Inputs:
%% o         - the bcm discretization structure
%% basic_sol - the basic solution centered at mx_src
%% f         - the coefficient vector of the solution we want to plot
%% t         - the receiver measurement index corresponding to the 
%%             time at which we want to plot the wave u^f
%% --------------------------------------------------------------------
%% Optional Inputs:
%% output_path - what directory we want to save the solution in 
%% file_name   - what we want the file to be called
%% color_axis  - range of values available for colors
%% --------------------------------------------------------------------
%% Outputs:
%% out  - the values of the jth temporal basis function evaluated on
%%        the t-coordinates
%% --------------------------------------------------------------------

if not(o.transl_invar_flag)
    error('Not translation invariant');
end

basic_sol = reshape(basic_sol, o.mt_rec, o.nx_sol, o.ny_sol);

[basic_sol, xs_sol, ys_sol] = pad_basic_sol(o, basic_sol);

[Xs,Ys] = meshgrid(xs_sol,ys_sol);
Xs = Xs';
Ys = -Ys';

Mx = length(xs_sol);
Ny = length(ys_sol);

nt_src = o.nt_src;
nx_src = o.nx_src;
f = reshape(f, nt_src, nx_src);

ut = zeros(Mx, Ny);

for j=1:nt_src
    % Delay the solution by (j-1) t_separations:
    indt = t - o.ratio_t_separation_dt*(j - 1);
    if indt > 1
        tmpt = squeeze(basic_sol(indt,:,:));
        for k=1:nx_src
            tmp = zeros(Mx, Ny);
            translx = -(nx_src-1)/2 + (k-1);
            if translx < 0
  	      tmp(1:Mx+translx, :) = tmpt(-translx+1:Mx, :);
            else
              tmp(translx+1:end, :) = tmpt(1:Mx-translx, :);
            end            
            ut = ut + f(j,k) * tmp;
        end
    end
end

if exist('output_path','var')
    save([output_path '/' file_name '.mat'],'ut');
end

if display_attached()    
    %% matlab can generate nice images if a display is not attached
    surf(Xs, Ys, ut);
    shading interp;
    view(0, 90);
    axis equal;
    axis tight;    
    colorbar;
    if exist('color_axis', 'var')
        caxis(color_axis);
    end
    if exist('output_path','var')
        print('-dpng',[output_path '/' file_name '.png']);
    end
else
    %% matlab cannot generate nice images if a display is not attached
    imagesc(flipud(ut'));
    axis equal;
    axis tight;    
    colorbar;
    if exist('color_axis', 'var')
        caxis(color_axis);
    end
    if exist('output_path','var')
        print('-dpng',[output_path '/' file_name '.png']);
    end
end

end

