function out = proj_basis(o, f)

%  This is the L^2 inner product (f, b) where the basis function b
%  is a Gaussian pulse both in space and time, ie b(x,t) = h(x)g(t).
%  In space the integral is approximated by a Riemman sum with Mwidth_x points.
%  In time the integral is approximated by a Riemann sum with Mwidth_t points. 
    
    nt_src = o.nt_src;
    mt_rec = o.mt_rec;
    nx_src = o.nx_src;
    nx_rec = o.nx_rec;

    % OLD:
    %Mwidth_t  = o.ratiowtdt;
    %Mtransl_t = o.ratio_t_separation_dt;

    % OLD:
    %Mwidth_x  = o.ratiowxdx;
    %Mtransl_x = o.ratio_x_separation_dx;

    out = zeros(nt_src, nx_src);

    ts = o.ts_rec(1: mt_rec);
    xs = o.xs_rec;
    
    % Precompute the temporal basis functions
    gVals = zeros(nt_src, mt_rec);    
    for j = 1:nt_src
        gVals(j,:) = fun_basis_t(o, ts, j);
    end

    % Precompute the spatial basis functions
    hVals = zeros(nx_src, nx_rec);    
    for i = 1:nx_src
        hVals(i,:) = fun_basis_x(o, xs, i);
    end
    
    % Compute the integral: 
    %
    %     \int_0^T \int_\Gamma f(t,x) phi_{ij}(t,x) ds dt
    %
    out =  gVals * f * hVals';        
    out = o.dx * o.dt * out(:);
    o.dt * out(:);
    
end
