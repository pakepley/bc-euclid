function L_i_1 = compute_L(o, oR, i, R, S)

    % Control Prob solver params
    Nouter   =   300;    % maximum n outer GMRES iters
    Nrestart =    20;    % maximum n inner GMRES iters
    tol      =  1e-6;    % tolerance for GMRES 
    alpha    =  1e-4;    % Tikhonov regularization parameter
    o_inversion = set_inversion_params(Nouter, Nrestart, alpha, tol);

    % Control set \Gamma, we currently use full boundary
    tau  =       make_tau(o, R) ;
    mask =    make_mask(o, tau) ;
    chi  =         double(mask) ;

    L_i_1 = zeros(o.nt_src + 2, oR.nx_sol * oR.ny_sol);
    
    for k = 0 : (o.nt_src + 1)
        t_c     = o.ts_src(1) + o.t_separation * k;
        [rhs_integral, f_coeffs] = generate_mvng_rec_rhs(o, t_c, i);

        %% Coefficients of the control problem's solution
        h_coeffs = solve_bc(o, o_inversion, rhs_integral, tau);

        %% extract those in the last r units of time
        h_coeffs = h_coeffs(mask);
        
        L_i_1(k + 1, :) = S * h_coeffs;
    end

    L_i_1 = flipud(L_i_1);
end
