function o = get_matrices(o, save_path, matrix_path)
    
    if not(exist('matrix_path', 'var'))
        matrix_path = [save_path '/matrices'];
    end

    if or(not(exist([matrix_path '/invGK.mat'],'file')), ...
          not(exist([matrix_path '/invG.mat'],'file')))
        
        % in this case, we haven't constructed G^-1 K or G^-1 yet:
        fprintf('Building Matrices:\n');
        o = init_inversion(o, save_path, matrix_path);        
    else
        %% Load invG * K if we need to
        if not(isfield(o, 'coeffs_invGK'))
            % K has already been constructed. load it:
            fprintf(['Loading G^-1 * K from:\n' matrix_path '/coeffs_invGK.mat\n']);
            load([matrix_path '/invGK.mat']);
            o.coeffs_invGK = coeffs_invGK;
        end

        %% Load invG if we need to
        if not(isfield(o, 'coeffs_invG'))
            fprintf(['Loading G^-1 from:\n' matrix_path '/coeffs_invG.mat\n']);
            load([matrix_path '/invG.mat']);
            o.coeffs_invG = coeffs_invG;
        end

        %% Load G if we need to
        if not(isfield(o, 'coeffs_G'))            
            fprintf(['Loading G from:\n' matrix_path '/coeffs_G.mat\n']);
            load([matrix_path '/G.mat']);
            o.coeffs_G = coeffs_G;
        end

    end

end