setup_dir_flag         = true;
run_solver_flag        = true;

%% Include some helper functions and raytracers
addpath(['./io_facilities']);
addpath(['./compute_bdf']);
addpath(['./ray_trace']);
addpath(['./wvfld_reconstruction_perfect_data']);

src_dir = pwd();
user_home = getenv('HOME');

save_base = [user_home '/Computational_Data'];
if not(exist(save_base,'dir'))
    mkdir(save_base);
end

save_root = [save_base '/wvfld_reconstruction_exact'];
if not(exist(save_root, 'dir'))
    mkdir(save_root);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setup_dir_flag    
    save_path = [save_root '/' timestamp()];    
    mkdir(save_path);
    
    %% Save a snapshot of the source code
    save_source_code(save_path);

    % save parameters and directory
    if not(exist([save_path '/discr.mat'], 'file'))
        data_type          =    'ND';
        start_type         =  'early';
        spatial_separation =   0.025;
        L                  =     8.0;
        t_separation       =   0.025;
        T                  =    1.25;
        n_processors       =      18;
        c_code             =   '1.0';
        %c_code             =   '1 + x[1]';
        transl_invar_flag  =    true;
        domain_type        = 'half_space';
        
        
        addpath(['./' domain_type]);    
        if strcmp(domain_type, 'half_space')
            discr = init_params(save_path, data_type, start_type, T, ...
                                t_separation, L, spatial_separation, c_code, ...
                                n_processors, transl_invar_flag);
        else
            error('Disk case not yet implemented');
        end
        
        save_femparams(discr, save_path);
        discr = basis_x_normalizations(discr,save_path);       
        save([save_path '/discr.mat'], 'discr');
    end
elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');
    load([save_path '/discr.mat']);
    addpath(['./' discr.domain_type]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_solver_flag
  diary([save_path '/fem_diary.txt']);

  if not(exist('discr','var'))
      % get the discr from the save_path location
      load([save_path '/discr.mat']);
  end

  run_femsolver(discr, save_path, 'sol_probe_mode');
  diary off;
end 


