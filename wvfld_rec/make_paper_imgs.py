import sys
from numpy import *
import matplotlib.pyplot as plt
import matplotlib.patheffects as pe
from matplotlib.figure import figaspect
import scipy.io as sio
import true_rays_for_paper as tr

#####################################################################################
discr_file='/home/publius/Computational_Data/wvfld_rec_project/2017_04m_02d_151201/discr.mat'
discr_mat=sio.loadmat(discr_file)

# convert the c-code description of c to a function
c_code = discr_mat['discr']['c_code'][0][0][0]
c_code = 'def c(x): return {0}'.format(c_code)
exec c_code

# we will need these below:
xs_src = discr_mat['discr']['xs_src'][0][0][0]
ts_src = discr_mat['discr']['ts_src'][0][0][0]

# get some grid points and plot the true solution on them
xs_sol = discr_mat['discr']['xs_sol'][0][0][0]
ys_sol = discr_mat['discr']['ys_sol'][0][0][0]
XX,YY = meshgrid(xs_sol, ys_sol)
pp = row_stack((XX.flatten(), YY.flatten()))
ZZ3 = c(pp)
ZZ3 = ZZ3.reshape(XX.shape)

# set colorbar range to min and max of range
clo = min(ZZ3.flatten())
chi = max(ZZ3.flatten())

#####################################################################################
results_file='/home/publius/Computational_Data/wvfld_rec_project/2017_04m_02d_151201/wvfld_rec_data/caps_h_0.1000_alpha_1.0000e-05/results_i_61_181_j_2_25/final_data.mat'
results_mat=sio.loadmat(results_file)

xs_int = results_mat['xs_int']
ys_int = results_mat['ys_int']
c_est = results_mat['c_est']

pp2 = row_stack((xs_int.flatten(), ys_int.flatten()))
ZZ2 = c(pp2)
ZZ2 = ZZ2.reshape(xs_int.shape)

# load the inversion grid ranges, and convert matlab indexing to python's:
os = results_mat['os']
ilo, ihi = int(os['ilo'])-1, int(os['ihi'])-1
jlo, jhi = int(os['jlo'])-1, int(os['jhi'])-1
nxx, nyy = int(os['nxx']), int(os['nyy'])

# the following are magic numbers for plotting
xlo, xhi = -2.0, 2.0
ylo, yhi = 0, 1.25
n_contours = 1000

def plot_scene(X, Y, Z, vmin=clo, vmax=chi):
	fig, ax = plt.subplots()
	cax = ax.contourf(X, Y, Z,
					  n_contours, 
					  #extent=[xlo, xhi, ylo, yhi],
					  #cmap = plt.get_cmap('Blues_r'),					  
					  #cmap = plt.get_cmap('Greys'),					  
					  cmap = plt.get_cmap('Blues'),
					  #cmap = plt.get_cmap('viridis'),
					  #cmap = plt.get_cmap('inferno'),
					  vmin = clo, 
					  vmax = chi)
	# fig.colorbar(cax, fraction=0.046, pad=0.04, aspect=7.25)
	ax.set_aspect('equal')
	ax.axis([xlo, xhi, ylo, yhi])
	ax.set_ylim([yhi, ylo])
	return fig, ax

def scatter_plot(X, Y):
	fig, ax = plt.subplots()
	cax = ax.scatter(X, Y, s=5, alpha=0.5)
	ax.set_aspect('equal')
	ax.axis([xlo, xhi, ylo, yhi])
	ax.set_ylim([yhi, ylo])
	return fig, ax


fig, ax = scatter_plot(xs_int[::2,::2], ys_int[::2,::2])
fig.savefig('estimated_points.png', bbox_inches='tight')

fig, ax = plot_scene(XX, YY, ZZ3)
fig.savefig('true_c.png', bbox_inches='tight')

fig, ax = plot_scene(xs_int, ys_int, c_est)
fig.savefig('estimated_c.png', bbox_inches='tight')

plt.ion()
fig2, ax2 = plt.subplots()
w, h  = 1.5 * 2.1547, 2.1547
fig2.set_size_inches(w,h)
ax2.axis([ts_src[jlo], ts_src[jhi], 0.75 * clo, chi])

jrng = range(jlo,jhi+1)
irng = range(ilo,ihi+1)

abcs = ['(a)','(b)','(c)']
for i, k in enumerate([21-1,41-1,61-1]):
	# add the ray gamma to the plot, output to file, and remove the ray to replot
	l, = ax.plot(xs_int[:,k],ys_int[:,k], 'r', 
				 path_effects=[pe.Stroke(linewidth=2, foreground='k'), pe.Normal()])
	ax.annotate(abcs[i], 
				xy = (xs_int[-1,k], ys_int[-1,k]), 
				xytext = (xs_int[-1,k] - .04, ys_int[-1,k] + .11))				

fig.savefig('rays.png'.format(k+1), bbox_inches='tight')
fig.canvas.draw()


fig, ax = plot_scene(xs_int, ys_int, c_est)
fig.savefig('estimated_c.png', bbox_inches='tight')

# for k in range(0,nxx+1, 10):
for k in [21-1,41-1,61-1]:
	# add the ray gamma to the plot, output to file, and remove the ray to replot
	l, = ax.plot(xs_int[:,k],ys_int[:,k], 'r', 
				 path_effects=[pe.Stroke(linewidth=2, foreground='k'), pe.Normal()])
	# linewidth=1.2)
	fig.savefig('ray_at_base_{0}.png'.format(k+1), bbox_inches='tight')
	fig.canvas.draw()
	l.remove()


	# plot the estimated value against the true values
	la, = ax2.plot(ts_src[jrng], c_est[:,k], 'r^', markersize=7.5, alpha =0.5)
	lb, = ax2.plot(ts_src[jrng], ZZ2[:, k], 'b', linewidth=2)
	fig2.canvas.draw()
	fig2.savefig('compare_along_slice_{0}.png'.format(k+1), bbox_inches='tight')
	lb.remove()
	la.remove()
	

# Plot true vs estimated
fig, ax = scatter_plot(xs_int[::2,::2], ys_int[::2,::2])
fig.savefig('estimated_points.png', bbox_inches='tight')
tr.add_background_to_plot(fig, ax, -2.0, 2.0, 0.0, 1.25, 100, 100, with_colorbar=False);	
T = .70 + 0.5 * (4 * .025) - 0.025
n_rays = 61
n_steps = int(ceil(T / .0125))
t_step = T / n_steps
print t_step
t_stride = 0.05
isochrone_separation = t_stride
stride = int(t_stride / t_step)
n_isochrones = int(T /  isochrone_separation)
#t_stride = T / stride
# t_lo = first t plotted + 0.5 * h
t_lo = .125 + 0.5 * (4 * 0.025)
lo = int((t_lo / T) * n_steps)
print n_isochrones, stride, t_step, t_stride,  lo * t_step

data = tr.add_normals_to_plot(fig, ax, T, n_rays, n_steps, -1.5, 1.5);
tr.add_isochrones_to_plot(fig, ax, data, n_steps, stride, lo=lo);
fig.savefig('./TRIP_BNC_2.png', bbox_inches='tight')
