function make_wvfld_rec_images(results_root, Ctrue)
    % load in the variables saved here:
    load([results_root '/results.mat']);
    
    % images
    image_path = [results_root '/images'];
    conditional_mkdir([results_root '/images']);

    % coordinate ranges for plots 3 and 6
    axis_range_3and6 = [-1.5 1.5 0.0 1.0];

    % true wavespeed plotted on the interpolated/estimated points
    ZZ2=Ctrue(xs_int(:),ys_int(:));
    ZZ2=reshape(ZZ2,os.nyy,os.nxx);

    % true wavespeed plotted on a grid of points
    [XX,YY]=meshgrid(discr2.xs_sol,discr2.ys_sol);
    ZZ3=Ctrue(XX(:),YY(:));
    ZZ3=reshape(ZZ3,discr2.ny_sol,discr2.nx_sol);

    % max of the true wavespeed in the full grid
    clo=min(ZZ3(:));
    chi=max(ZZ3(:));
    
    % plot 1
    figure(1);
    subplot(2,1,1);
    imagesc(xs_est); colorbar; 
    xlabel('ix'); ylabel('iy'); 
    title(['x estimated']);
    subplot(2,1,2);
    imagesc(xs_int); colorbar; 
    xlabel('ix'); ylabel('iy'); 
    title(['x estimated, then interpolated']);
    print('-dpng', [image_path '/xs_est.png']);


    % plot 2
    figure(2);
    subplot(2,1,1);
    imagesc(ys_est); colorbar; 
    xlabel('ix'); ylabel('iy'); 
    title(['y estimated']);
    subplot(2,1,2);
    imagesc(ys_int); colorbar; 
    xlabel('ix'); ylabel('iy'); 
    title(['y estimated, then interpolated']);
    print('-dpng', [image_path '/ys_est.png']);

    % plot 3
    figure(3);
    subplot(2,1,1);
    scatter(xs_est(:), ys_est(:), '.'); 
    xlabel('x');  ylabel('y'); 
    title(['scatter plot of estimated points']); 
    axis equal;
    axis(axis_range_3and6);
    subplot(2,1,2);
    scatter(xs_int(:), ys_int(:), '.'); 
    xlabel('x'); ylabel('y'); 
    title(['scatter plot of est/interpolated points']); 
    axis equal;
    axis(axis_range_3and6);
    print('-dpng', [image_path '/coord_transform_estimated.png']);

    % plot 4
    figure(4); 
    imagesc(c_est); 
    colorbar;
    title('c_estimated at x(y_i, s_j)');
    xlabel('source position (y_i)');
    ylabel('arc-length along geodesic (s_j)');
    print('-dpng', [image_path '/c_grid_estimates.png']);

    % plot 5
    figure(5);
    subplot(3,1,1); imagesc(ZZ2); caxis([clo chi]); colorbar; 
    subplot(3,1,2); imagesc(c_est); caxis([clo chi]); colorbar
    subplot(3,1,3); imagesc(abs(ZZ2-c_est)); colorbar

    % plot 6
    figure(6);
    subplot(3,1,1);
    surf(XX,YY,ZZ3);
    view(0,90);
    caxis([clo chi]);
    colorbar;
    shading interp;
    title('c true');
    axis(axis_range_3and6);

    subplot(3,1,2);
    surf(xs_int, ys_int, c_est);
    view(0,90);
    caxis([clo chi]);
    colorbar;
    shading interp;
    title('c estimated');
    axis(axis_range_3and6);

    subplot(3,1,3);
    surf(xs_int, ys_int, abs(c_est - ZZ2));
    view(0,90);
    colorbar;
    shading interp;
    axis(axis_range_3and6);
    title('difference');    
    print('-dpng', [image_path '/c_comparison.png']);


    slice_image_path = [image_path '/c_in_slices'];    
    conditional_mkdir(slice_image_path);

    for k= 1:os.nxx    
        figure(7);
        x_pos = discr2.xs_src(os.ilo + k - 1);
        
        subplot(3,1,1);
        surf(xs_int, ys_int, c_est);
        axis(axis_range_3and6); 
        hold on;
        view(0,90);
        plot3(xs_int(:,k), ys_int(:,k), c_est(:,k)+.01, 'Color', 'red');
        title(sprintf('Source Position x=%f', x_pos)); 
        hold off;
        
        subplot(3,1,2);
        plot(discr2.ts_src(os.jlo:os.jhi), ZZ2(:,k), ...
             discr2.ts_src(os.jlo:os.jhi), c_est(:,k)); 
        xlabel('s');
        title(sprintf('Wavespeed at x(y,*) where y=%f',x_pos));
        legend('True', 'Recovered');
        axis([0.1 0.8 (0.9*clo) (1.1*chi)]); 
        
        subplot(3,1,3);
        plot(discr2.ts_src(os.jlo:os.jhi), abs(ZZ2(:,k) - c_est(:,k))./ZZ2(:,k));
            
        xlabel('s');
        title('Relative Absolute Error');
        axis([0.1 0.8 0.0 0.1]); 
        
        print('-dpng', ...
              sprintf([slice_image_path '/slice_comparison_%d.png'], k));
    end

end