if not(exist([save_path '/c_at_ps_sol.mat']))
    system(['python ' src_dir '/half_space/compute_c_on_points.py ' save_path]);
end

if not(exist([save_path '/figures/']))
    system(['mkdir ' save_path '/figures']);
end

% compute < u^{f_ij}, phi> for phi = x
phi = @(x,y) ones(size(x));
int_bndry = rhs_for_wvfld_rec(discr, Lambda, @(x)(ones(size(x))), @(x)(zeros(size(x))));
int_inter = exact_rhs_for_wvfld_rec(discr, save_path, phi);

figure(1);
cc = [min([min(int_bndry), min(int_inter)]), max([max(int_bndry), max(int_inter)])];
subplot(3,1,1); imagesc(reshape(int_bndry, discr.nt_src, discr.nx_src)); 
caxis(cc); colorbar;
subplot(3,1,2); imagesc(reshape(int_inter,  discr.nt_src, discr.nx_src));
caxis(cc); colorbar;
subplot(3,1,3); imagesc(reshape(int_bndry - int_inter, ...
                        discr.nt_src, discr.nx_src)); colorbar;
subplot(3,1,1); title('\langle u^{f_{ij}}, \phi\rangle: \phi = 1, From boundary integrals');
subplot(3,1,2); title('From interior integrals');
subplot(3,1,3); title('Difference');
print('-dpng', [save_path '/figures/integral_ufij_1.png']);

% compute < u^{f_ij}, phi> for phi = x
phi = @(x,y) x;
int_bndry = rhs_for_wvfld_rec(discr, Lambda, @(x)(x), @(x)(zeros(size(x))));
int_inter = exact_rhs_for_wvfld_rec(discr, save_path, phi);

figure(2);
cc = [min([min(int_bndry), min(int_inter)]), max([max(int_bndry), max(int_inter)])];
subplot(3,1,1); imagesc(reshape(int_bndry, discr.nt_src, discr.nx_src));
caxis(cc); colorbar;
subplot(3,1,2); imagesc(reshape(int_inter,  discr.nt_src, discr.nx_src));
caxis(cc); colorbar;
subplot(3,1,3); imagesc(reshape(int_bndry - int_inter, ...
                        discr.nt_src, discr.nx_src)); colorbar;
subplot(3,1,1); title('\langle u^{f_{ij}}, \phi\rangle: \phi = x, From boundary integrals');
subplot(3,1,2); title('From interior integrals');
subplot(3,1,3); title('Difference');
print('-dpng', [save_path '/figures/integral_ufij_x.png']);

% compute < u^{f_ij}, phi> for phi = y
phi = @(x,y) y;
int_bndry = rhs_for_wvfld_rec(discr, Lambda, @(x)(zeros(size(x))), @(x)(-ones(size(x))));
int_inter = exact_rhs_for_wvfld_rec(discr, save_path, phi);

figure(3);
cc = [min([min(int_bndry), min(int_inter)]), max([max(int_bndry), max(int_inter)])];
subplot(3,1,1); imagesc(reshape(int_bndry, discr.nt_src, discr.nx_src));
caxis(cc); colorbar;
subplot(3,1,2); imagesc(reshape(int_inter,  discr.nt_src, discr.nx_src));
caxis(cc); colorbar;
subplot(3,1,3); imagesc(reshape(int_bndry - int_inter, ...
                        discr.nt_src, discr.nx_src)); colorbar;
subplot(3,1,1); title('\langle u^{f_{ij}}, \phi\rangle: \phi = y, From boundary integrals');
subplot(3,1,2); title('From interior integrals');
subplot(3,1,3); title('Difference');
print('-dpng', [save_path '/figures/integral_ufij_y.png']);
