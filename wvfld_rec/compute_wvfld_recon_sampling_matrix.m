function S = compute_wvfld_recon_sampling_matrix(o, alpha, save_path, h, ...
                                                 ilo, ihi, jlo, jhi)
    %% how many points in the interior we want samples at
    ni = (ihi-ilo)+1
    nj = (jhi-jlo)+1
    nsamples = ni * nj
    
    %% how many basis functions in our basis
    nbasis = o.nx_src * o.nt_src;
    
    S = zeros(nsamples, nbasis);
    
    for i=ilo:ihi
        for j=jlo:jhi
            psi_i_j = compute_cap_control(o, alpha, save_path, ...
                                          i, j, h);
            % which row to put data in
            k = (i-ilo)*nj + (j-jlo+1);

            %% psi integrals
                S(k,:) = psi_i_j';
        end
    end
end