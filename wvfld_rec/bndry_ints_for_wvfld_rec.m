function [x_bndry_ints, y_bndry_ints] = bndry_ints_for_wvfld_rec(o, Lambda)

    % compute < u^{f_ij}, phi> for phi = x
    phi = @(x,y) x;
    x_bndry_ints = rhs_for_wvfld_rec(o, Lambda, ...
                                     @(x)(x), ...
                                     @(x)(zeros(size(x))));
    
    % compute < u^{f_ij}, phi> for phi = y
    phi = @(x,y) y;
    y_bndry_ints = rhs_for_wvfld_rec(o, Lambda, ...
                                     @(x)(zeros(size(x))), ...
                                     @(x)(-ones(size(x))));
end