function [c_est, dx_ds, dy_ds, xs_int, ys_int] = ...
        compute_wavespeed(o, os, xs_est, ys_est, p_in, w)
    
    if nargin==5
        w=ones(size(os.jlo:os.jhi))';
    end
    
    [nr, nc] = size(xs_est);

    tt = o.ts_src(os.jlo:os.jhi)';

    dx_ds = zeros(nr,nc);
    dy_ds = zeros(nr,nc);
      
    xs_int = zeros(nr,nc);
    ys_int = zeros(nr,nc);
    
    %% Fit vertically :
    for i = 1:nc,         
        xx = xs_est(1:end,i);
        PP = csaps(tt, xx, p_in, [], w); 
        xs_int(:,i) = ppval(PP, tt); 
        p_der = fnder(PP,1); 
        dx_ds(:,i) = ppval(p_der,tt); 
    end    
    for i = 1:nc, 
        yy = ys_est(1:end,i);
        PP = csaps(tt, yy, p_in, [], w);
        ys_int(:,i) = ppval(PP, tt); 
        p_der = fnder(PP,1); 
        dy_ds(:,i) = ppval(p_der,tt); 
    end    
    
    c_est = sqrt(dx_ds.^2 + dy_ds.^2);
end