function out_integral = exact_rhs_for_wvfld_rec(o, save_path, phi)
%% --------------------------------------------------------------------
%% Function: exact_rhs_for_wvfld_rec(o, save_path, phi, p_nu_phi, Lambda)
%% Use:      compute "exact" right-hand side integral for the boundary control
%%           problem for the wavefield recovery problem, using the true
%%           wavefield
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% save_path  - the save path
%% phi        - function handle for a harmonic function
%% --------------------------------------------------------------------
%% Outputs:
%% b_integral - the integrals of the funbtion b(t,x) = T - t 
%% --------------------------------------------------------------------    

    % the goal is to compute:
    %     integral( phi(x,y) * u^{f_ij}(T,x,y) * (1/c(x,y))^2 dx dy )
                
    % load c pre-evaluated on the ps_sol
    tmppath = [save_path '/c_at_ps_sol.mat'];
    if not(exist(tmppath, 'file'))
        % compute and load the values of c on ps_sol
        system(['python ./' o.domain_type '/compute_c_on_points.py ' save_path]);
    end
    tmp = load(tmppath);
    c_at_ps_sol = tmp.c_at_ps_sol;
    inv_c_sq = c_at_ps_sol.^(-2);

    % grid of points:
    [xs, ys] = meshgrid(o.xs_sol, o.ys_sol);
    xs = xs';
    ys = ys';
        
    % evaluate phi on these points:
    phis = phi(xs,ys);
    
    % integration grid steps
    dx = abs(o.xs_sol(2) - o.xs_sol(1));
    dy = abs(o.ys_sol(2) - o.ys_sol(1));

    % output:
    out_integral = zeros(o.nt_src, o.nx_src);
    
    for i = 1:o.nx_src
        % load the solution evaluated on ts_rec(1:mt_rec) * xs_sol * ys_sol
        u_f_i1 = get_sol(o, [save_path '/sol'], i);
        
        for j = 1:o.nt_src
            % delay u^{f_i1} to get u^{f_ij}:
            t_index = o.mt_rec - o.ratio_t_separation_dt*(j - 1);
            u_f_ij = squeeze(u_f_i1(t_index, :, :));
            
            % compute the product:
            out_integral(j,i)= sum(sum(u_f_ij .* phis .* inv_c_sq)) * dx * dy;
        end
    end
    
    out_integral = out_integral(:);
    
end