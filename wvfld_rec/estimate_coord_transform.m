function [xs_est, ys_est, x_bndry_ints, y_bndry_ints] = ...
                   estimate_coord_transform(o, os, Lambda, S)

    % estimate the coordinate transformation
    [x_bndry_ints, y_bndry_ints] = bndry_ints_for_wvfld_rec(o, Lambda);
    xs_est = S * x_bndry_ints;
    ys_est = S * y_bndry_ints;
    
    % reshape into gridded data
    xs_est = reshape(xs_est, os.nyy, os.nxx);
    ys_est = reshape(ys_est, os.nyy, os.nxx);

end