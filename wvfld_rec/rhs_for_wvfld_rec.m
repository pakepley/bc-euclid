function out_integral = rhs_for_wvfld_rec(o, Lambda, phi, p_nu_phi)
%% --------------------------------------------------------------------
%% Function: rhs_for_wvfld_rec(o, phi, p_nu_phi)
%% Use:      compute right-hand side integral for the boundary control
%%           problem for the wavefield recovery problem
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% Lambda     - the Lambda values, if not included, this will
%%              be loaded from file which is slower
%% phi        - function handle for a harmonic function
%% p_nu_phi   - function handle for a the normal derivative of phi 
%%              on the boundary set
%% --------------------------------------------------------------------
%% Outputs:
%% b_integral - the integrals of the funbtion b(t,x) = T - t 
%% --------------------------------------------------------------------
            
    % we only need Lambda on [0,T]:
    Lambda = Lambda(1:o.mt_rec, :, :);
    
    % Lambda needs to be rescaled by the basis normalization:
    for i = 1:o.nx_src
        Lambda(:,:,i) = Lambda(:,:,i) * o.normalization_Lambda(i);
    end
    
    % times and points where we evaluate:
    ts = o.ts_rec(1:o.mt_rec);
    xs = o.xs_rec;

    % compute the spatial basis functions:
    fxs = zeros(o.nx_src, o.nx_rec);
    for i =1:o.nx_src
        fxs(i,:) = fun_basis_x(o, xs, i);
    end
    
    % compute the temporal basis functions:
    fts = zeros(o.nt_src, o.mt_rec);
    for j = 1:o.nt_src
        fts(j,:) = fun_basis_t(o, ts, j);
    end
                
    % compute the integral of 
    %
    %                g_ij(t,x) * b(t) 
    %
    % where:
    %
    %   g_ij(t,x) = b(t) * f_ij(t,x) * phi(x) - b(t) * u^{f_ij}(t,x) * d[phi(x)]/d_nu
    %          =           h_ij(t,x)          -            k_ij(t,x)

    % evaluate the function b(t) on ts_rec(1:o.mt_rec) = [0,T]
    b_t = (o.T -  ts)';
    
    % evaluate phi and p_nu_phi on xs_rec:
    phis = phi(o.xs_rec);
    p_nu_phis = p_nu_phi(o.xs_rec);
       
    % compute the integral of h_ij 
    h_integral = (fts * b_t * o.dt) * (phis * fxs' * o.dx);

    % reserve space for the k_integral:
    k_integral = zeros(o.nt_src, o.nx_src);
    
    % compute the following x integral:
    %
    %      integral( d[phi(x)]/d_nu * u^{f_ij}(t,x) dx)
    %
    % do this by contracting along axis 1
    k_x_int = squeeze(sum(bsxfun(@times, Lambda, p_nu_phis),2) * o.dx);
    
    % now we integrate in t for each basis function:   
    for j=1:o.nt_src
        transl = o.ratio_t_separation_dt*(j - 1);
        
        % delay the results in time 
        tmp = zeros(o.mt_rec, o.nx_src);
        tmp(transl+1:end, :) = k_x_int(1:o.mt_rec-transl,:);

        % integrate in time:
        k_integral(j,:) = (b_t' * tmp) * o.dt;
    end

    out_integral = h_integral(:) - k_integral(:);
    
end