function psi_i_j = compute_cap_control(o, alpha, save_path, i, j, h)
    output_root = [save_path '/wvfld_rec_data'];    
    conditional_mkdir(output_root);
    
    output_path = sprintf([output_root '/caps_h_%.04f_alpha_%.04e'], h, alpha);
    conditional_mkdir(output_path);
    
    s = o.ts_src(j);
    y = o.xs_src(i);
    
    tau1_file = sprintf([output_path '/tau1_%d.mat'], j);        
    psi_i_j_file = sprintf([output_path '/psi_%d_%d.mat'], i, j);

    b_integrals = generate_rhs_for_bdf(o);

    if not(exist(tau1_file, 'file'))
        fprintf(sprintf('building tau_%d\n', j));
        tau1 = make_tau(o, s);        
        [f1, mask1] = solve_bc(o, alpha, b_integrals, tau1);    
        save(tau1_file, 'f1', 'mask1', '-v7');
    end

    if not(exist(psi_i_j_file, 'file'))
        fprintf(sprintf('building psi_%d_%d\n', i,j));
        load(tau1_file);
        tau2 = make_tau(o, s, y, h);
        [f2, mask2] = solve_bc(o, alpha, b_integrals, tau2);
        psi_i_j = f2 - f1;
        vol_i_j = psi_i_j' * b_integrals;
        psi_i_j = psi_i_j / vol_i_j;       
        save(psi_i_j_file, 'psi_i_j', 'vol_i_j', '-v7');
    else
        load(psi_i_j_file);
    end
end