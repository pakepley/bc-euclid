function [vol, f, mask] = solve_bc_for_distances(o, os, tau_type,s,y,h,z,r)
%% --------------------------------------------------------------------
%% Function: solve_bc_for_distances
%% Use     : solve the bc problem for the appropriate tau used in the
%%           distance estimation procedure
%% --------------------------------------------------------------------
%% Inputs:
%% o        - the bcm discretization structure
%% os       - inversion parameters
%% tau_type - the type of tau needed, an integer between 1 and 4
%%              case 1: flat portion     M(s1_\Gamma)
%%              case 2: target cap       M(s1_\Gamma) \cup cap(y,s,h)
%%              case 3: variable cap     M(s1_\Gamma) \cup cap(z,s,r)
%%              case 4: both caps        M(s1_\Gamma) \cup 
%%                                         cap(y,s,h) \cup cap(z,s,r)
%% s        - the height of the flat portion 
%% y,z      - wave-cap base points. y refers to the target cap 
%%            base-point and z refers to the variable cap base-point
%% h,r      - heights for up to two wave-caps. When both are used, s+h is 
%%            the target cap radius and s+r is the variable cap radius
%% --------------------------------------------------------------------
%% Outputs:
%% vol  - the volume of the domain of influence associated with tau
%% f    - the control which generates the approximately constant
%%        wave-field
%% mask - the support constraint mask
%% --------------------------------------------------------------------
    
    switch tau_type
      case 1
        % flat portion 
        tau = make_tau(o,s);      
      case 2
        % target cap
        tau = make_tau(o,s,y,h);
      case 3
        % variable cap
        tau = make_tau(o,s,z,r);
      case 4
        % both caps
        tau = make_tau(o,s,y,h,z,r);
    end    
    
    %% Compute the integrals of b against basis
    b_integral = generate_b_integral(o);
    
    %% Solve
    [f, mask] = solve_bc(o, os, b_integral, tau);

    % compute the volume
    vol = sum(b_integral .* f);
    
end