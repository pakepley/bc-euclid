function generate_data_for_distances(o, os, distance_path, s, y, h, j_lo, j_hi, stride, mode)
%% --------------------------------------------------------------------
%% Function: generate_data_for_distances
%% Use     : generates data for distance computaiton procedure
%% --------------------------------------------------------------------
%% Inputs:
%% o        - the bcm discretization structure
%% os       - inversion parameters
%% j_lo     - low index for base-point z  (integer between 1 and o.mx_src)
%% j_hi     - high index for base-point z (integer between 1 and o.mx_src)
%% j_stride - how many src points between each basepoint (integer)
%% s        - the height of the flat portion 
%% y        - the target cap base-point
%% h        - h is the target cap height, s+h is the target cap radius
%% --------------------------------------------------------------------
%% Optional Inputs:
%% mode     - a string informing us how to compute the data. By default,
%%            we will sweep through every possible r-value. If mode is
%%            set to 'minimal', then we will only make the minimum volume
%%            computations required to estimate the distances
%% --------------------------------------------------------------------
    
    if not(exist('mode','var'))
        mode = 'default';
    end
    
    % Where the distance data will be saved
    if not(exist(distance_path,'dir'))
        mkdir(distance_path);
    end
    
    % file to save the volumes we compute:
    vol_file = [distance_path '/vols.mat'];
    
    % subdirectories to hold bcm solution data
    tau_roots = cell(1,4);
    for i = 1:4
        tau_roots{i} = [distance_path sprintf('/tau%d', i)];        
        if not(exist(tau_roots{i},'dir'))
            mkdir(tau_roots{i});
        end
    end
    
    try
        % Load stuff from file. If it fails, at least one hasn't been
        % computed so we should compute whatever we need       
        warning off;
        load(vol_file,'vol1','vol2','vol_target');
        warning on;
    catch
        % Set up directory structure for the flat and target taus and solve
        % the bc problem    
        for i = 1:2
            output_path = [tau_roots{i} '/data'];
            if not(exist(output_path, 'dir'))
                mkdir(output_path);
            end        
            % set dummy values, NOTE: THESE WILL NOT BE USED just appear here
            % so I can make a call to solve_bc_for_distances without
            % re-writing its function signature        
            z = y;
            r = h; 
            tau_type = i;
            data_file = [output_path '/data.mat'];
            if not(exist(data_file, 'file'))
                %% We haven't computed the wavefield yet
                [vol, f, mask] = solve_bc_for_distances(o, os, tau_type,s,y,h,z,r);
                save(data_file, 'vol', 'f', 'mask');
            else
                load(data_file, 'vol');                  
            end
            if i == 1
                vol1 = vol;
            elseif i == 2
                vol2 = vol;
            end
        end
    
        % Compute the volume of the target cap
        vol_target = vol2 - vol1;
        if not(exist(vol_file, 'file'))
            save(vol_file, 'vol1', 'vol2', 'vol_target');
        end
    end
    
 
    try
        warning off;
        load(vol_file,'vols3','vols4','vol_overlaps');
        warning on;
    end
    % Set up directory structure for the variable and combined taus and solve    
    % the bc problem
    for j = j_lo : stride : j_hi
        disp(sprintf('Working on experiment %d', j));
        
        % if we've already computed some volumes from a different
        % run, we may already have computed what we need now.
        if exist('vol_overlaps', 'var')
            if length(vol_overlaps) < j
                vol_overlaps{j} = {}; 
            end
        else
            vol_overlaps{j} = {};
        end

        max_r_index = floor((o.T - s) / o.t_separation);
        [k, is_done] = get_next_r_index(o, j, vol_target, vol_overlaps, ...
                                           max_r_index, mode);
        not_done = not(is_done);

        %% Loop through the r(k) until the termination criterion is met
        while ( not_done )
            disp(sprintf('Computing with r(%d)', k));
            
            % Set the variable cap base point and r(k)
            z = o.xs_src( j );    
            r = o.t_separation * k;          % t_separation is the
                                             % basis resolution
            for i =3:4
                output_root = ...
                    [tau_roots{i} sprintf('/experiment_%.03d', j)];
                if not(exist(output_root, 'dir'))
                    mkdir(output_root);
                end            
                
                output_path = [output_root '/data'];
                if not(exist(output_path, 'dir'))
                    mkdir(output_path);
                end
                
                % Set the tau_type
                tau_type = i;                    % 3 --> variable cap
                                                 % 4 --> combined
                
                % Solve the BC problem
                data_file = [output_path sprintf('/data_%.04d.mat',k)];
                if not(exist(data_file, 'file'))                    
                    [vol, f, mask] = solve_bc_for_distances(o, os, tau_type, ...
                                                            s,y,h,z,r);                        
                    save(data_file, 'vol', 'f', 'mask');
                else
                    load(data_file, 'vol');
                end                 
                
                if i == 3
                    vols3{j}{k} = vol;
                elseif i == 4
                    vols4{j}{k} = vol;
                end
            end
            
            % Compute the volume of the intersection between target and
            % variable caps
            vol_overlaps{j}{k} = vol2 + vols3{j}{k} - vol1 - vols4{j}{k};
            save(vol_file, 'vols3', 'vols4', 'vol_overlaps', '-append');
            [k, is_done] = get_next_r_index(o, j, vol_target, vol_overlaps, ...
                                               max_r_index, mode);
            
            not_done = not(is_done);

        end        
    end
end