function plot_script_for_example_images(o, save_path, base_name, x_axis_vec, ...
                                        font_size, label_axes, y,z, lambda_range)
    % image types:
    image_types = {'tau1', 'tau2', 'tau3', 'tau4'};
            
    for i = 1:length(image_types)
        directories{i} = [save_path '/experiment_example/' image_types{i} ...
                          '/'];
        conditional_mkdir(directories{i});
        
        ut_mat_file{i} = [directories{i} '/' image_types{i} '.mat'];
        fs_mat_file{i} = [directories{i} '/fs_' image_types{i} '.mat'];
        data_mat_file{i} = [directories{i} '/data.mat'];
        
        %% get the source function f
        data{i} = load(data_mat_file{i});            
        fs{i}   = data{i}.f; 

        % generate ut matfile if necessary
        if not(exist(ut_mat_file{i}, 'file'))
            if not(exist('basic_sol', 'var'))
                basic_sol = dlmread(sprintf([save_path '/sol/sol_%d/sol_%d.txt'], ...
                                            o.mx_src, o.mx_src));
            end
            plot_sol_from_probes(o, basic_sol, fs{i}, o.mt_rec, directories{i}, ...
                                 image_types{i});
        end

        % generate fs matfile if necessary
        if not(exist(fs_mat_file{i}, 'file'))
            plot_boundary_function(o, fs{i}, directories{i}, ['fs_' image_types{i}]);
        end

        ut_mat{i} = load(ut_mat_file{i});
        uts{i} = ut_mat{i}.ut;
        
        fs_mat{i} = load(fs_mat_file{i});
        fs_yys{i} = fs_mat{i}.yy;
    end    
    
    % ranges for color axis
    ut_lo = min(min(cell2mat(uts)));
    ut_hi = max(max(cell2mat(uts)));

    % ranges for color axis
    %fs_lo = min(min(cell2mat(fs_yys)))
    %fs_hi = max(max(cell2mat(fs_yys)))
    
    fs_lo = -100;
    fs_hi = 400;
    
    fudge_y = -.005;
    
    if exist('lambda_range', 'var')
        aa = linspace(lambda_range(1),lambda_range(2));
        bb = zeros(size(aa)) + fudge_y;
        cc = ut_hi * ones(size(aa));
    end

    
    for j = 1:length(uts)
        close all;
        figure(1);
        hold on;
        plot_sol_from_ut(o, uts{j});
        if exist('lambda_range', 'var')
            line(aa,bb,cc,'LineWidth',1.5,'Color','Black');
        end
        if j == 2 || j == 4
            plot3(y, fudge_y, ut_hi, '.', 'MarkerSize',16, 'Color','Black');
        end
        if j == 3 || j == 4
            plot3(z, fudge_y, ut_hi, '.', 'MarkerSize',16, 'Color','Black');
        end
        hold off;
        ax1 = gca(1);
        fig1 = get(ax1, 'children');

        
        figure(2);
        plot_boundary_function_from_yy(o, fs_yys{j});
        %line(aa,bb,'LineWidth',2);
        ax2 = gca(2);
        fig2 = get(ax2, 'children');                
        
        figure(3);
        s1 = subplot(2,1,1);
        daspect([1 1 1]);
        colorbar;
       
        s2 = subplot(2,1,2);
        daspect([1 1 1])
        colorbar;        
        
        copyobj(fig1,s1);
        copyobj(fig2,s2);
        
        set(s1,'XLim',x_axis_vec);
        set(s1,'YLim',[-o.T,0.0]);
        set(s1,'CLim',[ut_lo, ut_hi]);
        set(s1, 'FontSize', font_size);        
        
        set(s2,'XLim',x_axis_vec);
        set(s2,'YLim',[0.0, o.T]);
        set(s2,'CLim',[fs_lo, fs_hi]);
        set(s2, 'FontSize', font_size);                

        if label_axes            
            %set( get(s1,'XLabel'), 'String', ...
            %                  'x');
            %set( get(s2,'XLabel'), 'String', ...
            %                  'x');
            set( get(s1,'YLabel'), 'String', 'Depth' );
            set( get(s2,'YLabel'), 'String', 'Time' );
            pos1=get(get(s1,'Ylabel'),'Pos')
            pos2=get(get(s2,'Ylabel'),'Pos')
            x1 = min(pos1(1), pos2(1))       
            set(get(s1,'Ylabel'),'Pos', [x1 pos1(2) pos1(3)])
            set(get(s2,'Ylabel'),'Pos', [x1 pos2(2) pos2(3)])
        end
        
        colormap(s1, parula);
        colormap(s2, parula);
        
        close(1); close(2);
        
        figure(3);
                
        [directories{j} '/ut_' image_types{j} '.png']
        print('-dpng', [directories{j} '/' base_name '_' image_types{j} '.png']);            
    end
end
