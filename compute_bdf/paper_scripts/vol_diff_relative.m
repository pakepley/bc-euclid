function vol_diff_relative(o, dist_path, y,s,h, j_lo, j_hi, ...
                           stride, nr_max)
    % Estimate the distances
    zs = o.xs_src;
    
    % Get the volume info
    load([dist_path '/vols.mat']);
    
    % make a directory to hold the volume 
    output_path = [dist_path '/relative_vols'];
    conditional_mkdir(output_path);
    
    for j = j_lo : stride : j_hi,
        %% the rs used:
        rs_exp = o.x_separation * (1 : nr_max);

        %% the corresponding vols (assume they were generated in the
        %%    "all" mode)
        vol_overlaps_j = vol_overlaps{j};
        vol_overlaps_j = cell2mat(vol_overlaps_j);
        vol_overlaps_j = vol_overlaps_j(1 : nr_max);        
        
        nr_true = 1000;
        rs_plot = linspace(0, rs_exp(end), nr_true);
        %% compute the true volumes
        true_vol = zeros(1, nr_true);
        for i =1:nr_true
            %% %% for debugging							 
            %% the_variable_cap = circ_ysh(x,zs(j),rs_plot(i),s);
            %% plot(x,the_line, x,the_fixed_cap, x,the_variable_cap);
            %% print('-dpng',['./junk_images/test_' num2str(i) '.png']);
            %% what's actually interesting
            %
            true_vol(i) = bounded_vol(y,h,zs(j),rs_plot(i),s);
        end
        
        vol_target_est  = vol_target;
        vol_target_true = max(true_vol);
        
        % %%Print absolute volumes
        % clf();
        % hold on;
        % %% reference volumes:
        % plot(rs_plot,true_vol, 'Color', [0 .5 .5], 'LineWidth', 2.5);
        % %% Computed volumes
        % scatter(rs_exp,vol_overlaps_j,[],[0 .5 0], 'd', ...
        %         'MarkerEdgeColor',[0 0 1.0],...
        %         'LineWidth', 1.5)        
        % set(gca,'FontSize',20)
        % fprintf(['Saved in: \n' output_path sprintf('/atest%02d.png',j)])
        % print('-dpng',[output_path sprintf('/atest%02d.png',j)]);
        % xlabel('r')
        % ylabel('Vol_\mu(cap_{\Gamma}(y,s,r)) / Vol_\mu(cap_{\Gamma}(y,s,h))')
        % hold off;    
        
        %%Print relative results:
        clf();
        hold on;
        %% Reference volumes:
        rs_plot
        true_vol / vol_target_true
        plot(rs_plot, true_vol/vol_target_true, 'Color', [0 .5 .5], 'LineWidth', 2.5);
        scatter(rs_exp,vol_overlaps_j/vol_target_est, 49,[0 .5 0], 'd', ...
                'MarkerEdgeColor',[0 0 1.0], ...
                'LineWidth', 1.5)
        axis([0 rs_plot(nr_true) -0.2 1.01]);        
        set(gca,'FontSize',24)
        xlabel('r')
        ylabel('m_{overlap} / m_{target}');
        fprintf(['Saved in: \n' output_path sprintf('/rel_vols_exp_%02d.png',j)])
        print('-dpng',[output_path sprintf('/rel_vols_exp_%02d.png',j)]);
        hold off;
    end
end