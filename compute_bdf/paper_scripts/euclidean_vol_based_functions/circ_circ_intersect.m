function c = circ_circ_intersect(y,h,z,r,s)
    if y <= z
        c = .5 *(z+y) + (.5*(r^2-h^2) +s*(r-h))/(y-z);
    else
        c = .5 *(y+z) + (.5*(h^2-r^2) +s*(h-r))/(z-y);
    end
end