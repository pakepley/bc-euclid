function plot_script_for_corner_comparison(o, save_path)
    % image types:
    image_types = {'disk', 'flat', 'flat_and_cap', 'diff'};
            
    for i = 1:length(image_types)
        directories{i} = [save_path '/control_solutions/' image_types{i} ...
                          '/'];
        conditional_mkdir(directories{i});
        
        ut_mat_file{i} = [directories{i} '/' image_types{i} '.mat'];
        data_mat_file{i} = [directories{i} '/data.mat'];
        
        %% get the source function f
        if i <= 3
            data{i} = load(data_mat_file{i});            
            fs{i}   = data{i}.f; 
        elseif i == 4
            fs{i}   = data{3}.f - data{2}.f; 
        end

        % generate ut if necessary
        if not(exist(ut_mat_file{i}, 'file'))
            if not(exist('basic_sol', 'var'))
                basic_sol = dlmread(sprintf([save_path '/sol/sol_%d/sol_%d.txt'], ...
                                            o.mx_src, o.mx_src));
            end
            plot_sol_from_probes(o, basic_sol, fs{i}, o.mt_rec, directories{i}, ...
                                 image_types{i});
        end
        ut_mat{i} = load(ut_mat_file{i});
        uts{i} = ut_mat{i}.ut;
    end    
    
    % ranges for color axis
    ut_lo = min(min(cell2mat(uts)));
    ut_hi = max(max(cell2mat(uts)));
        
    for j = 1:length(uts)
        figure(j);
        if strcmp(o.c_code, '1.0')
            y_lo = -.8;
        elseif strcmp(o.c_code, '1 + x[1]')
            y_lo = -1.2;
        end
        plot_sol_from_ut(o, uts{j}, directories{j}, ['ut_' image_types{j}], ...
                         [ut_lo, ut_hi], [-1.8, 1.8, y_lo, 0], 12);
                   
    end
end


            % if j >= 2
            %     x0 = -discr.L;
            %     y0 = .5;
            %     r = .15;
            %     t = linspace(0,2*pi,1000);
            %     plot3(x0 + r*cos(t), y0 + r*sin(t),cmax*ones(size(t)), 'black', 'LineWidth', 1.25); 
            %     pause(1.);
            %     if j == 3
            %         x0 = -.5;
            %         y0 = .5;
            %         plot3(x0 + r*cos(t), y0 + r*sin(t),cmax*ones(size(t)), 'black', 'LineWidth', 1.25);                             
            %     end
            % end
   