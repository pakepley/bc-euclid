function x_y_s = compute_xys(o, y, s)
%% --------------------------------------------------------------------
%% Function: compute_xys(o, y, s)
%% Use     : compute the point x(y,s)
%% --------------------------------------------------------------------
%% Inputs:
%% o     - the bcm discretization structure
%% y     - the y-coordinate of the starting point (y,0)
%% s     -  the travel-time into the interior along the normal ray 
%%         starting from y 
%% --------------------------------------------------------------------
%% Outputs:
%% x_y_s - the point x(y,s)
%% --------------------------------------------------------------------

    if strcmp(o.c_code, '1.0')
        x_y_s = [y, s];
    elseif strcmp(o.c_code, '1 + x[1]')
        y_y_s = exp(s) - 1;
        x_y_s = [y, y_y_s];
    else
        error(['compute_xys.m: ' ...
               'Wave-speed c_code not found. This point x(y,s) is easy'...
               'to approximate by ray-tracing, but I am lazy and have' ...
               'and have not done this yet!']);
    end


end