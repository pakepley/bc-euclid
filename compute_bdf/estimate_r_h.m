function [r_h, i_lo] = estimate_r_h(o, vol_target, vol_overlaps)
%% --------------------------------------------------------------------
%% Function: estimate_r_h(o, vol_target, vol_overlaps)
%% Use     : compute the value r_h from the experimental data
%% --------------------------------------------------------------------
%% Inputs:
%% o            - the bcm discretization structure
%% vol_target   - the volume of the target cap
%% vol_overlaps - the volumes of the intersection between the target
%%                and variable caps. (Note, this may be a cell-struct
%%                or a vector.)
%% --------------------------------------------------------------------
%% Optional Outputs:
%% r_h          - the estimated value of r_h used in the distance 
%%                computation. WARING: IF THERE IS NO INDEX i FOR 
%%                WHICH THE FOLLOWING HOLDS:
%%
%%                vol_overlaps(i) <= .5 * vol_target <= vol_ovelaps(i+1)
%%
%%                THEN THERE WILL BE **NO** RETURN VALUE, SINCE THE
%%                DATA IS INSUFFICIENT TO ESTIMATE r_h.
%% i_lo         - the index i in the condition defining r_h (see r_h)
%% --------------------------------------------------------------------
    
    if iscell(vol_overlaps)
        n_vols = length(vol_overlaps);
        for i = 1:n_vols
            if isempty(vol_overlaps{i})
                vol_overlaps{i} = 0.0;
            end
        end
        % convert to a vector if it's a cell struct        
        vol_overlaps = cell2mat(vol_overlaps);
    end
    
    % normalize the volume overlaps
    relative_vol_overlaps = vol_overlaps / vol_target;
        
    % compute the value of r_h. The vealue of r_h is found by
    % interpolating between indices as described above
    if and(min(relative_vol_overlaps) <= .5, ...
           max(relative_vol_overlaps) >= .5)

        % closest index from below and relative vol
        i_lo = find(relative_vol_overlaps <= .5, 1, 'last' );
        v_lo = relative_vol_overlaps(i_lo);
        % r_lo = o.ts_src(i_lo);
        r_lo = o.t_separation * i_lo;
        
        % closest index from above and relative vol, note that we cannot
        % have the index being larger than the number of source positions!        
        i_hi = min( find(relative_vol_overlaps >= .5, 1 ), o.nt_src);
        v_hi = relative_vol_overlaps(i_hi);
	% r_hi = o.ts_src(i_hi);
        r_hi = o.t_separation * i_hi;
 
        % use linear interpolation to estimate what r should be
        if not(v_hi == v_lo)
            r_h = r_lo + (.5 - v_lo) * ((r_hi - r_lo) / (v_hi - v_lo));
        else
            r_h = r_lo;
        end
    % there is one additional case, vol_overlaps(1) >= .5 vol_target
    % so we're interpolating between 0 and vol_overlaps(1), we check
    % for that here, although we have to be careful to first check that
    % relative_vol_overlaps is not empty
    elseif not(isempty(relative_vol_overlaps))
        if relative_vol_overlaps(1) >= .5
            i_lo = 1;
            r_lo = o.t_separation * i_lo;
            r_h  = .5 * r_lo;
        end
    end
    
    % Handle the possiblity that r_h couldn't be computed
    if not(exist('i_lo','var'))
        r_h = {};   
        i_lo = {};
    end
    
end
