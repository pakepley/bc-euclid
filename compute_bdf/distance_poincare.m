function d_p_q = distance_poincare(o,p,q)    
    d_p_q = acosh( 1 + ( (p(1)-q(1))^2 + (p(2)-q(2))^2 ) / (2 * (1+p(2)) ...
                                                      * (1+q(2))));
end

    