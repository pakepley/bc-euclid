function [distances, zs] = compute_exact_distances_poincare( ...
    o,save_path, s,y,zs)
%% --------------------------------------------------------------------
%% Function: compute_exact_distances(o, save_path, s, y, z)
%% Use     : compute exact distances and save them to the file,
%%           distance_path/exact_distances.mat
%% --------------------------------------------------------------------
%% Inputs:
%% o             - the bcm discretization structure
%% save_path     - the location where everything is saved
%% distance_path - the work directory where we compute distances
%% i             - the index  of the point y = xs_src[i]
%% s             - the height of the flat portion 
%% --------------------------------------------------------------------
%% Outputs:
%% distances     - the distances we computed, i.e. d(zs, x(y,s))
%% --------------------------------------------------------------------


%% for the ray x(y,*) will travel normally from y up to x(y,s)
%% and will actually just move vertically. the final y-coordinate
%% at x(y,s) is given by:
yf = exp(s) - 1;

%% the distance between two points in the metric determined by
%% c = y + 1 is given by:
%%
%%  d((ax,ay), (bx,by)) 
%%         = acosh( 1 + ( (bx-ax)^2 + (by-ay)^2 ) / (2(1+ay)(1+by)))
%%
%% so we compute d((y,yf), (z[i],0)) by:
%distances = zeros(lenth(z));
%for i = 1:length(z):
%    distances(i) = acosh( 1 + ( (y - z(i))^2 + (yf)^2 ) / (2 * (1+yf)));
distances = acosh( 1 + ( (y - zs).^2 + (yf)^2 ) / (2 * (1+yf)));
%end

end